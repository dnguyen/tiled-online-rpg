﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using TiledGame.Entities;
using TiledLib;
using TiledGame.GUI;
using TiledGame.Utils;
using TiledGame.Network;

namespace TiledGame.Components
{
    class PlayerInput
    {
        private KeyboardState kState;
        private KeyboardState oldKState;
        private MouseState mState;
        // TODO: Customizable keymap - store keymap on the server
        private Dictionary<Keys, string> keymap = new Dictionary<Keys,string>() {
            { Keys.I, "inventory" }
        };

        public void Update(GameTime gameTime, GameClient client, GUI.GUI gui)
        {
            kState = Keyboard.GetState();
            mState = Mouse.GetState();

            client.Player.IsMoving = false;
            if (!client.Talking)
            {
                // Movement
                if (kState.IsKeyDown(Keys.Down))
                {
                    client.Player.Move(CharacterDirection.DOWN);
                }
                else if (kState.IsKeyDown(Keys.Up))
                {
                    client.Player.Move(CharacterDirection.UP);
                }
                else if (kState.IsKeyDown(Keys.Left))
                {
                    client.Player.Move(CharacterDirection.LEFT);
                }
                else if (kState.IsKeyDown(Keys.Right))
                {
                    client.Player.Move(CharacterDirection.RIGHT);
                }

                if (kState.IsKeyDown(Keys.A) && oldKState.IsKeyUp(Keys.A))
                {
                    client.Player.Attack(AttackType.RANGED);
                }

                if (kState.IsKeyDown(Keys.B) && oldKState.IsKeyUp(Keys.B))
                {
                    client.Player.Attack(AttackType.MELEE);
                }

                if (kState.IsKeyDown(Keys.OemTilde) && oldKState.IsKeyUp(Keys.OemTilde))
                {
                    client.netClient.SendMessage(PacketCreator.EquipItem(client, 1), Lidgren.Network.NetDeliveryMethod.ReliableOrdered);
                }

                // UI Windows
                foreach (Keys key in kState.GetPressedKeys())
                {
                    if (keymap.ContainsKey(key) && oldKState.IsKeyUp(key))
                    {
                        if (gui.Windows.ContainsKey(keymap[key]))
                        {
                            if (gui.Windows[keymap[key]].Visible)
                            {
                                gui.Windows[keymap[key]].Visible = false;
                                gui.Windows[keymap[key]].Focused = false;
                            }
                            else
                            {
                                gui.Windows[keymap[key]].Visible = true;
                                gui.Windows[keymap[key]].Enabled = true;
                                gui.Windows[keymap[key]].Focused = true;
                            }
                        }
                        else
                        {
                            Window newWindow = UserInterfaceSingleton.Instance.GetWindow(keymap[key]);
                            gui.Windows.Add(keymap[key], newWindow);
                            Console.WriteLine(newWindow.Texture.Width);
                        }

                    }
                }
            }
            oldKState = kState;
        }

    }
}
