﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using TiledGame.Entities;
using TiledLib;

namespace TiledGame.Engine
{
    class Camera
    {
        private Vector2 position;
        public Vector2 Position { get { return position; } }
        public float X { get { return position.X; } }
        public float Y { get { return position.Y; } }

        public void MoveCamera(Character chara, Map map) 
        {
            position.X = chara.Position.X - Game1.WINDOW_WIDTH / 2;
            position.Y = chara.Position.Y- Game1.WINDOW_HEIGHT / 2;

            Vector2 cameraMax = new Vector2(
                map.Width * map.TileWidth - Game1.WINDOW_WIDTH,
                map.Height * map.TileHeight - Game1.WINDOW_HEIGHT);
            position = Vector2.Clamp(position, Vector2.Zero, cameraMax);
        }

    }
}
