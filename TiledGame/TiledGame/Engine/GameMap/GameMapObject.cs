﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TiledGame.Engine.GameMap
{
    public enum MapObjectType
    {
        PLAYER = 0,
        ENEMY = 1
    }

    class GameMapObject
    {
        private int objectId;
        private Vector2 position;
        private MapObjectType objectType;

        #region Properties
        public int ObjectId { get { return objectId; } set { objectId = value; } }
        public Vector2 Position { get { return position; } set { position = value; } }
        public MapObjectType ObjectType { get { return objectType; } set { objectType = value; } }
        public float X { get { return position.X; } set { position.X = value; } }
        public float Y { get { return position.Y; } set { position.Y = value; } }
        #endregion
    }
}
