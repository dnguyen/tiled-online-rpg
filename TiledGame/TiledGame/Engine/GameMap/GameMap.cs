﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TiledGame.Entities;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using TiledGame.Utils;
using TiledLib;

namespace TiledGame.Engine.GameMap
{
    class GameMap
    {
        private int id;
        private string name;
        private Map tiledMap;
        private Dictionary<int,Character> players;
        private Dictionary<int, Enemy> enemies;

        public int Id { get { return id; } set { id = value; } }
        public string Name { get { return name; } set { name = value; } }
        public Map TiledMap { get { return tiledMap; } set { tiledMap = value; } }
        public Dictionary<int, Character> Players { get { return players; } set { players = value; } }
        public Dictionary<int, Enemy> Enemies { get { return enemies; } set { enemies = value; } }

        public GameMap(int id, Map tiledMap_)
        {
            this.id = id;

            tiledMap = tiledMap_;
            name = tiledMap.Properties["name"].RawValue;

            players = new Dictionary<int, Character>();
            enemies = new Dictionary<int, Enemy>();
        }

        public void AddPlayer(int charid, Character player)
        {
            players.Add(charid, player);
        }

        public void MovePlayer(Character player)
        {
            players[player.Id].X = player.X;
            players[player.Id].Y = player.Y;
            players[player.Id].Direction = player.Direction;
        }

        public Character GetPlayerById(int id)
        {
            foreach (KeyValuePair<int, Character> player in players)
            {
                if (player.Value.Id == id)
                {
                    return player.Value;
                }
            }
            return null;
        }

        public void Update(GameTime gameTime)
        {
            if (players.Count > 0)
            {
                foreach (KeyValuePair<int, Character> character in players)
                {
                    character.Value.Update(gameTime);
                }
            }

            if (enemies.Count > 0)
            {
                foreach(KeyValuePair<int, Enemy> enemy in enemies) {
                    enemy.Value.Update(gameTime);
                }
            }


        }

        public void Draw(SpriteBatch spriteBatch, Rectangle visibleArea)
        {
            ResourceManager.Maps[id].TiledMap.Draw(spriteBatch, visibleArea);
            if (players.Count > 0)
            {
                foreach (KeyValuePair<int, Character> character in players)
                {
                    character.Value.Draw(spriteBatch);
                }
            }

            if (enemies.Count > 0)
            {
                foreach(KeyValuePair<int, Enemy> enemy in enemies)
                {
                    enemy.Value.Draw(spriteBatch);
                }
            }
            MapObjectLayer objects = tiledMap.GetLayer("portals") as MapObjectLayer;
            MapObject portal = objects.GetObject("portal1");
            spriteBatch.Draw(ResourceManager.portalTexture, new Vector2(portal.Bounds.X, portal.Bounds.Y), Color.White);
        }
    }
}
