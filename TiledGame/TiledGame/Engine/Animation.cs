﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TiledGame.Engine
{
    class Animation
    {
        private SpriteSheet spriteSheet;
        private int frameIndex;
        private float timeElapsed;
        private bool isLooping = false;
        private float timeToUpdate = 0.05f;
        public int FramesPerSecond
        {
            set { timeToUpdate = (1f / value); }
        }

        public SpriteSheet Sheet { get { return spriteSheet; } }
        public int FrameIndex { get { return frameIndex; } }

        public Animation(SpriteSheet sheet, int fps, bool loop)
        {
            spriteSheet = sheet;
            FramesPerSecond = fps;
            isLooping = loop;
        }

        public void Update(GameTime gameTime)
        {
            timeElapsed += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (timeElapsed > timeToUpdate)
            {
                timeElapsed -= timeToUpdate;

                if (frameIndex < spriteSheet.Frames.Count - 1)
                    frameIndex++;
                else if (isLooping)
                    frameIndex = 0;
            }
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 position)
        {
            spriteBatch.Draw(spriteSheet.Sheet, position, spriteSheet.Frames[frameIndex], Color.White);
        }
    }
}
