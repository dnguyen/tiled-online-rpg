﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace TiledGame.Engine
{
    class SpriteSheet
    {
        private Texture2D sheet;
        private List<Rectangle> frames;
        private int startX;
        private int startY;
        private int width;
        private int height;
        private int frameWidth;
        private int frameHeight;

        public Texture2D Sheet { get { return sheet; } }
        public List<Rectangle> Frames { get { return frames; } }
        public int FrameWidth { get { return frameWidth; } }
        public int FrameHeight { get { return frameHeight; } }

        public SpriteSheet(Texture2D sheet_, int startX_, int startY_, int width_, int height_, int frameW, int frameH)
        {
            sheet = sheet_;
            width = width_;
            height = height_;
            frameWidth = frameW;
            frameHeight = frameH;
            startX = startX_;
            startY = startY_;
            frames = new List<Rectangle>();
            CreateFrames();
        }

        private void CreateFrames()
        {
           // Console.WriteLine("Across: " + width / frameWidth);
            //Console.WriteLine("Up/Down: " + height / frameHeight);
            for (int y = startY; y < height; y += frameHeight)
            {
                for (int x = startX; x < width; x += frameWidth)
                {
                    frames.Add(new Rectangle(x, y, frameWidth, frameHeight));
                    //Console.WriteLine("X:" + x + " Y:" + y + " W:" + frameWidth + " H:" + frameHeight);
                }
            }
        }
    }
}
