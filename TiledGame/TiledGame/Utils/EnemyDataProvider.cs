﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using TiledGame.Entities;
using TiledGame.Engine;
using Microsoft.Xna.Framework.Graphics;

namespace TiledGame.Utils
{
    class EnemyDataProvider
    {
        private static EnemyDataProvider instance;
        private ContentManager content;
        private Dictionary<int, Dictionary<EnemyDirection, Animation>> enemyAnimationsCache;

        public ContentManager Content { get { return content; } set { content = value; } }
        public Dictionary<int, Dictionary<EnemyDirection, Animation>> EnemyAnimationsCache { get { return enemyAnimationsCache; } set { enemyAnimationsCache = value; } }

        public static EnemyDataProvider Instance { get { return instance; } set { instance = value; } }

        const int spriteSize = 32;

        public EnemyDataProvider(ContentManager content_)
        {
            content = content_;
            enemyAnimationsCache = new Dictionary<int, Dictionary<EnemyDirection, Animation>>();
        }

        public Dictionary<EnemyDirection, Animation> GetEnemyAnimations(int id)
        {
            if (!enemyAnimationsCache.ContainsKey(id))
            {
                Texture2D spriteSheet = content.Load<Texture2D>("Sprites\\Monsters\\" + id.ToString());

                // Create animations for that spritesheet
                Dictionary<EnemyDirection, Animation> enemyAnimations = new Dictionary<EnemyDirection, Animation>();

                enemyAnimations.Add(EnemyDirection.DOWN, new Animation(
                new SpriteSheet(spriteSheet, 0, 0, spriteSize * 3, spriteSize, spriteSize, spriteSize),
                5, true));

                enemyAnimations.Add(EnemyDirection.LEFT, new Animation(
                new SpriteSheet(spriteSheet, 0, spriteSize, spriteSize * 3, spriteSize * 2, spriteSize, spriteSize),
                5, true));

                enemyAnimations.Add(EnemyDirection.RIGHT, new Animation(
                new SpriteSheet(spriteSheet, 0, spriteSize * 2, spriteSize * 3, spriteSize * 3, spriteSize, spriteSize),
                5, true));

                enemyAnimations.Add(EnemyDirection.UP, new Animation(
                new SpriteSheet(spriteSheet, 0, spriteSize * 3, spriteSize * 3, spriteSize * 4, spriteSize, spriteSize),
                5, true));

                enemyAnimationsCache.Add(id, enemyAnimations);

                return enemyAnimations;
            }
            else
            {
                return enemyAnimationsCache[id];
            }
        }
    }
}
