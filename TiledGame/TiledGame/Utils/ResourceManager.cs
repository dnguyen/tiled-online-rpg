﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TiledLib;
using Microsoft.Xna.Framework.Content;
using System.IO;
using Microsoft.Xna.Framework.Graphics;
using TiledGame.Engine;
using TiledGame.Entities;
using System.Xml;
using TiledGame.Entities.Items;
using TiledGame.Engine.GameMap;

namespace TiledGame.Utils
{
    class ResourceManager
    {
        private ContentManager content;
        public static Dictionary<int, GameMap> Maps;
        public static Dictionary<string, Texture2D> SpriteSheets;
        public static Dictionary<string, SpriteFont> Fonts;
        public static Dictionary<string, Texture2D> GuiElements;
        public static Dictionary<string, Dictionary<CharacterDirection, Animation>> CharacterAnimations;

        public static Dictionary<string, Texture2D> debugTextures;

        public static Texture2D portalTexture;

        private const int spriteSize = 32;
        public ResourceManager(ContentManager content_)
        {
            content = content_;

            Maps = new Dictionary<int, GameMap>();
            SpriteSheets = new Dictionary<string, Texture2D>();
            Fonts = new Dictionary<string, SpriteFont>();
            GuiElements = new Dictionary<string, Texture2D>();
            CharacterAnimations = new Dictionary<string, Dictionary<CharacterDirection, Animation>>();

            debugTextures = new Dictionary<string, Texture2D>();
            debugTextures.Add("test_projectile", content.Load<Texture2D>("Sprites\\test_projectile"));

            portalTexture = content_.Load<Texture2D>("Sprites\\Objects\\portal");

            LoadMaps();
            LoadSpriteSheets();
            LoadSpriteFonts();
            LoadGuiElements();
            LoadCharacterAnimations();
        }

        public void LoadCharacterAnimations()
        {
            // Loop through each file in the sprites\characters content folder for character spritesheets
            foreach (string characterSpriteSheet in Directory.GetFiles(content.RootDirectory + "\\Sprites\\Characters"))
            {
                string CharacterSpriteSheetFileName = Path.GetFileName(characterSpriteSheet.Remove(characterSpriteSheet.Length - 4));
                // Create the spritesheet
                Texture2D spriteSheet = content.Load<Texture2D>("Sprites\\Characters\\" + CharacterSpriteSheetFileName);

                // Create animations for that spritesheet
                Dictionary<CharacterDirection, Animation> charAnimations = new Dictionary<CharacterDirection, Animation>();

                charAnimations.Add(CharacterDirection.DOWN, new Animation(
                new SpriteSheet(spriteSheet, 0, 0, spriteSize * 3, spriteSize, spriteSize, spriteSize),
                5, true));

                charAnimations.Add(CharacterDirection.LEFT, new Animation(
                new SpriteSheet(spriteSheet, 0, spriteSize, spriteSize * 3, spriteSize * 2, spriteSize, spriteSize),
                5, true));

                charAnimations.Add(CharacterDirection.RIGHT, new Animation(
                new SpriteSheet(spriteSheet, 0, spriteSize * 2, spriteSize * 3, spriteSize * 3, spriteSize, spriteSize),
                5, true));

                charAnimations.Add(CharacterDirection.UP, new Animation(
                new SpriteSheet(spriteSheet, 0, spriteSize * 3, spriteSize * 3, spriteSize * 4, spriteSize, spriteSize),
                5, true));

                CharacterAnimations.Add(CharacterSpriteSheetFileName, charAnimations);
            }
        }

        public void LoadGuiElements()
        {
            foreach (string guiFile in Directory.GetFiles(content.RootDirectory + "\\GUIElements"))
            {
                string guiName = Path.GetFileName(guiFile.Remove(guiFile.Length - 4));
                GuiElements.Add(guiName, content.Load<Texture2D>("GUIElements\\" + guiName));
            }
        }

        public void LoadSpriteFonts()
        {
            foreach (string fontFile in Directory.GetFiles(content.RootDirectory + "\\Fonts"))
            {
                string fontName = Path.GetFileName(fontFile.Remove(fontFile.Length - 4));
                Fonts.Add(fontName, content.Load<SpriteFont>("Fonts\\" + fontName));
            }
        }

        public void LoadSpriteSheets()
        {
            foreach (string sheetFile in Directory.GetFiles(content.RootDirectory + "\\Sprites"))
            {
                string sheetName = Path.GetFileName(sheetFile.Remove(sheetFile.Length - 4));
                SpriteSheets.Add(sheetName, content.Load<Texture2D>("Sprites\\" + sheetName));
            }
        }

        public void LoadMaps()
        {
            foreach (string mapFile in Directory.GetFiles(content.RootDirectory + "\\Maps"))
            {
                int mapId = int.Parse(Path.GetFileName(mapFile.Remove(mapFile.Length - 4)));
                Maps.Add(mapId, new GameMap(mapId, content.Load<Map>("Maps\\" + mapId)));
            }
        }

    }
}
