﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TiledGame.Entities.Items;
using Microsoft.Xna.Framework.Content;
using System.IO;
using Microsoft.Xna.Framework.Graphics;
using System.Xml;
using TiledGame.Engine;

namespace TiledGame.Utils
{
    class ItemDataProvider
    {
        private ContentManager content;

        private static ItemDataProvider instance;
        public static ItemDataProvider Instance { get { return instance; } set { instance = value; } }

        private Dictionary<int, RegularItem> regularItems;
        private Dictionary<int, Equipment> equipment;
        private Dictionary<int, Dictionary<EquipmentDirection, Animation>> equips;

        public Dictionary<int, RegularItem> RegularItems { get { return regularItems; } set { regularItems = value; } }
        public Dictionary<int, Equipment> Equipment_ { get { return equipment; } set { equipment = value; } }
        public Dictionary<int, Dictionary<EquipmentDirection, Animation>> Equips { get { return equips; } set { equips = value; } }

        private const int spriteSize = 32;

        public ItemDataProvider(ContentManager content_) {
            content = content_;
            regularItems = new Dictionary<int, RegularItem>();
            equipment = new Dictionary<int, Equipment>();
            equips = new Dictionary<int, Dictionary<EquipmentDirection, Animation>>();
        }

        public Dictionary<EquipmentDirection, Animation> GetEquipAnimations(int id)
        {
            Dictionary<EquipmentDirection, Animation> equipAnimations = new Dictionary<EquipmentDirection, Animation>();

            if (!equips.ContainsKey(id))
            {
                foreach (string directory in Directory.GetDirectories("Data\\Items\\Equipment"))
                {
                    string directoryName = directory.Remove(0, 5);

                    foreach (string equipXMLFile in Directory.GetFiles(directory))
                    {
                        string equipSpriteSheetFileName = Path.GetFileName(equipXMLFile.Remove(equipXMLFile.Length - 4));

                        if (id == int.Parse(equipSpriteSheetFileName))
                        {
                            Texture2D spriteSheet = content.Load<Texture2D>("Sprites\\" + directoryName + "\\" + equipSpriteSheetFileName);

                            equipAnimations.Add(EquipmentDirection.DOWN, new Animation(
                                new SpriteSheet(spriteSheet, 0, 0, spriteSize * 3, spriteSize, spriteSize, spriteSize), 5, true));
                            equipAnimations.Add(EquipmentDirection.LEFT, new Animation(
                                new SpriteSheet(spriteSheet, 0, spriteSize, spriteSize * 3, spriteSize * 2, spriteSize, spriteSize), 5, true));
                            equipAnimations.Add(EquipmentDirection.RIGHT, new Animation(
                                new SpriteSheet(spriteSheet, 0, spriteSize * 2, spriteSize * 3, spriteSize * 3, spriteSize, spriteSize), 5, true));
                            equipAnimations.Add(EquipmentDirection.UP, new Animation(
                                new SpriteSheet(spriteSheet, 0, spriteSize * 3, spriteSize * 3, spriteSize * 4, spriteSize, spriteSize), 5, true));

                            equips.Add(id, equipAnimations);
                            break;
                        }
                    }
                }
            }
            else
            {
                return equips[id];
            }
            return equipAnimations;
        }

        public RegularItem GetItemById(int id)
        {
            RegularItem item = null;

            if (!regularItems.ContainsKey(id))
            {
                foreach (string itemxml in Directory.GetFiles("Data\\Items\\Regular"))
                {
                    string itemFileName = Path.GetFileName(itemxml.Remove(itemxml.Length - 4));

                    if (id == int.Parse(itemFileName))
                    {
                        Texture2D sprite = content.Load<Texture2D>("Sprites\\Items\\Regular\\" + itemFileName);

                        XmlDocument doc = new XmlDocument();
                        doc.Load(itemxml);

                        XmlNode itemName = doc.GetElementsByTagName("name")[0];
                        XmlNode itemType = doc.GetElementsByTagName("type")[0];
                        item = new RegularItem(id, sprite, 0);
                        item.Name = itemName.InnerText;
                        item.Type = byte.Parse(itemType.InnerText);

                        regularItems.Add(item.Id, item);
                        break;
                    }

                }
            }
            else
            {
                item = regularItems[id];
            }

            return item;
        }

        public Equipment GetEquipById(int id)
        {
            Equipment equip = null;
            if (!equipment.ContainsKey(id))
            {
                foreach (string directory in Directory.GetDirectories("Data\\Items\\Equipment"))
                {
                    string directoryName = directory.Remove(0,5);

                    foreach (string equipXMLFile in Directory.GetFiles(directory))
                    {
                        string equipSpriteSheetFileName = Path.GetFileName(equipXMLFile.Remove(equipXMLFile.Length - 4));

                        if (id == int.Parse(equipSpriteSheetFileName))
                        {
                            Texture2D spriteSheet = content.Load<Texture2D>("Sprites\\" + directoryName+"\\" + equipSpriteSheetFileName);

                            XmlDocument doc = new XmlDocument();
                            doc.Load(equipXMLFile);

                            XmlNode equipName = doc.GetElementsByTagName("name")[0];
                            Console.WriteLine(equipName.InnerText);

                            EquipmentType equipType = (EquipmentType)Enum.Parse(typeof(EquipmentType), doc.GetElementsByTagName("type")[0].InnerText);

                            Dictionary<EquipmentDirection, Animation> equipAnimations = new Dictionary<EquipmentDirection, Animation>();

                            equipAnimations.Add(EquipmentDirection.DOWN, new Animation(
                                new SpriteSheet(spriteSheet, 0, 0, spriteSize, spriteSize, spriteSize, spriteSize), 5, true));
                            equipAnimations.Add(EquipmentDirection.LEFT, new Animation(
                                new SpriteSheet(spriteSheet, 0, spriteSize, spriteSize, spriteSize * 2, spriteSize, spriteSize), 5, true));
                            equipAnimations.Add(EquipmentDirection.RIGHT, new Animation(
                                new SpriteSheet(spriteSheet, 0, spriteSize * 2, spriteSize, spriteSize * 3, spriteSize, spriteSize), 5, true));
                            equipAnimations.Add(EquipmentDirection.UP, new Animation(
                                new SpriteSheet(spriteSheet, 0, spriteSize * 3, spriteSize, spriteSize * 4, spriteSize, spriteSize), 5, true));

                            equip = new Equipment(id, equipAnimations);
                            equip.Name = equipName.InnerText;
                            equipment.Add(id, equip);
                            break;
                        }
                    }
                }
            }
            else
            {
                equip = equipment[id];
            }
            return equip;
        }
    }
}
