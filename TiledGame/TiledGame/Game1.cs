using System;
using Lidgren.Network;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using TiledGame.GameStates;
using TiledGame.Network;
using TiledGame.Utils;
using TiledGame.GUI;

namespace TiledGame
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        ResourceManager resourceManager;

        public static int WINDOW_WIDTH = 800;
        public static int WINDOW_HEIGHT = 600;

        private GameClient client;

        KeyboardState keyboardState;
        KeyboardState oldKeyboardState;

        Screen activeScreen;
        GameScreen gameScreen;
        MenuScreen menuScreen;
        CharacterSelectionScreen charSelectScreen;

        public Screen ActiveScreen { get { return activeScreen; } set { activeScreen = value; } }

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            graphics.PreferredBackBufferHeight = WINDOW_HEIGHT;
            graphics.PreferredBackBufferWidth = WINDOW_WIDTH;
            graphics.ApplyChanges();

            NetPeerConfiguration config = new NetPeerConfiguration("TiledGameServer");
            client = new GameClient();

            client.netClient = new NetClient(config);
            client.netClient.Start();

            NetOutgoingMessage om = client.netClient.CreateMessage();
            om.Write(01); // connection packet
            client.netClient.Connect("127.0.0.1", 14242, om);

            Console.WriteLine("Client Started");
        }

        protected override void Initialize()
        {
            base.Initialize();
            UserInterfaceSingleton.Instance = new UserInterfaceSingleton(GraphicsDevice);
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            resourceManager = new ResourceManager(Content);
            ItemDataProvider.Instance = new ItemDataProvider(Content);
            EnemyDataProvider.Instance = new EnemyDataProvider(Content);

            menuScreen = new MenuScreen(this, spriteBatch, client);
            menuScreen.loginEvent += new LogInEventHandler(LogIn);
            menuScreen.afterLoginEvent += new AfterLoginHandler(AfterLogIn);
            Components.Add(menuScreen);
            menuScreen.Hide();

            charSelectScreen = new CharacterSelectionScreen(this, spriteBatch, client);
            charSelectScreen.SelectedCharacterEvent += new SelectedCharacterHandler(SelectedCharacter);
            Components.Add(charSelectScreen);
            charSelectScreen.Hide();

            gameScreen = new GameScreen(this, spriteBatch, client);
            Components.Add(gameScreen);
            gameScreen.LoadContent(Content);
            gameScreen.Hide();

            activeScreen = menuScreen;
            activeScreen.Show();
        }

        public void LogIn(LogInEventArgs e)
        {
            Console.WriteLine("User pushed login button");
            client.netClient.SendMessage(PacketCreator.RequestLoginAccount(client, e.Username, e.Password), NetDeliveryMethod.ReliableOrdered);
        }

        public void AfterLogIn()
        {
            // Request characters from server
            client.netClient.SendMessage(PacketCreator.RequestCharacter(client), NetDeliveryMethod.ReliableOrdered);

            menuScreen.Enabled = false;
            activeScreen.Hide();
            activeScreen = charSelectScreen;
            activeScreen.Show();
        }

        public void SelectedCharacter(SelectedCharacterEventArgs e)
        {
            activeScreen.Hide();
            activeScreen = gameScreen;
            activeScreen.Show(); 

            NetOutgoingMessage packet = client.netClient.CreateMessage();
            packet.Write((int)SendPacketIds.REQUEST_SPAWN_PLAYER);
            packet.Write((int)e.Id);
            client.netClient.SendMessage(packet, NetDeliveryMethod.ReliableOrdered);
        }

        protected override void UnloadContent()
        {
            //ResourceManager.maps[gameScreen.Player.CurrentMap.Name].Players.Remove(gameScreen.Player.Name);
            // Let other players in the map know that the player has logged out
            client.netClient.Shutdown("bye");
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();


            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);

            base.Draw(gameTime);
        }

        private bool CheckKey(Keys theKey)
        {
            return keyboardState.IsKeyUp(theKey) && oldKeyboardState.IsKeyDown(theKey);
        }
    }
}
