﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace TiledGame.GUI
{
    class Window : Control
    {
        private Dictionary<string, Control> controls;
        private Control focusedControl;
        private Rectangle TitleBounds;
        private bool dragging = false;
        private bool enableDragging = true;
        private Vector2 mousePos = Vector2.Zero;
        private Vector2 mouseOffset = Vector2.Zero;

        public Dictionary<string, Control> Controls { get { return controls; } set { controls = value; } }
        public bool EnableDragging { get { return enableDragging; } set { enableDragging = value; } }

        public Window(string name_, Vector2 pos, Vector2 size) : base(name_, pos, size)
        {
            controls = new Dictionary<string, Control>();
            TitleBounds = new Rectangle((int)X, (int)Y, (int)Width, 25);
        }

        public void Update(GameTime gameTime, MouseState mouseState, MouseState prevMouseState)
        {
            TitleBounds = new Rectangle((int)X, (int)Y, (int)Width, 25);

            mousePos = new Vector2(mouseState.X, mouseState.Y);

            if (mouseState.LeftButton == ButtonState.Released)
                dragging = false;

            // Check if mouse clicked a control
            if (ButtonState.Pressed == mouseState.LeftButton && ButtonState.Released == prevMouseState.LeftButton && !dragging)
            {
                if (mouseState.X >= X && mouseState.Y >= Y && mouseState.X <= X + Width && mouseState.Y <= Y + Height)
                {
                    Focused = true;
                    foreach (KeyValuePair<string, Control> control in controls)
                    {
                        if (ClickedControl(mouseState, control.Value))
                        {
                            focusedControl = control.Value;
                            control.Value.Focused = true;
                            control.Value.ControlClicked();
                        }
                        else
                        {
                            control.Value.Focused = false;
                        }
                    }
                }
                else
                {
                    Focused = false;
                }
            }

            if (enableDragging)
            {
                // Check for window dragging
                if (ButtonState.Pressed == mouseState.LeftButton)
                {
                    if (mouseState.X >= TitleBounds.X
                            && mouseState.Y >= TitleBounds.Y
                            && mouseState.X <= TitleBounds.X + TitleBounds.Width
                            && mouseState.Y <= TitleBounds.Y + TitleBounds.Height)
                    {
                        Focused = true;
                        dragging = true;
                        mouseOffset = Position - mousePos;
                    }
                }
            }

            if (focusedControl != null)
            {
                focusedControl.Update(gameTime);
            }

            if (dragging)
            {
                Move();
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, Position, Color.White);

            foreach (KeyValuePair<string, Control> control in controls)
            {
                if (control.Value.Visible)
                {
                    control.Value.Draw(spriteBatch);
                }
            }
            
        }

        public void AddControl(Control control)
        {
            if (!controls.ContainsKey(control.Name))
                controls.Add(control.Name, control);    
        }

        public void RemoveControl(string name)
        {
            if (controls.ContainsKey(name))
                controls.Remove(name);
        }

        public bool ClickedControl(MouseState mouseState, Control control)
        {
            if (mouseState.X >= control.X && mouseState.Y >= control.Y && mouseState.X <= control.X + control.Width && mouseState.Y <= control.Y + control.Height)
            {
                Console.WriteLine(control.Name + " clicked");
                return true;
            }
            else
            {
                return false;
            }
        }

        private void Move()
        {
            Position = mousePos + mouseOffset;

            // Update control positions
            foreach (KeyValuePair<string, Control> control in controls)
            {
                control.Value.Position = Position + control.Value.Offset;
            }
        }
    }
}
