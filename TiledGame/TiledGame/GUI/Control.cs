﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TiledGame.GUI
{
    class Control
    {
        private string name;
        private Texture2D texture;
        private Vector2 position = Vector2.Zero;
        private Vector2 size;
        private Vector2 offset;
        private bool visible;
        private bool enabled;
        private bool focused;
        private Rectangle bounds;
        private Control parent;

        public event ClickedEventHandler clickedEvent;
        public event FocusEventHandler focusEvent;
        public event UnFocusEventHandler unfocusEvent;

        public string Name { get { return name; } set { name = value; } }
        public Texture2D Texture { get { return texture; } set { texture = value; } }
        public Vector2 Position { get { return position; } set { position = value; } }
        public Vector2 Size { get { return size; } set { size = value; } }
        public float Width { get { return size.X; } set { size.X = value; } }
        public float Height { get { return size.Y; } set { size.Y = value; } }
        public bool Visible { get { return visible; } set { visible = value; } }
        public bool Enabled { get { return enabled; } set { enabled = value; } }
        public bool Focused {
            get { return focused; } 
            set { 
                focused = value;
                if (focused)
                {
                    if (focusEvent != null)
                        focusEvent();
                }
                else
                {
                    if (unfocusEvent != null)
                        unfocusEvent();
                }
            } 
        }

        public float X { get { return position.X; } set { position.X = value; } }
        public float Y { get { return position.Y; } set { position.Y = value; } }

        public Vector2 Offset { get { return offset; } set { offset = value; } }
        public float OffsetX { get { return offset.X; } set { offset.X = value; } }
        public float OffsetY { get { return offset.Y; } set { offset.Y = value; } }

        public Rectangle Bounds { get { return bounds; } set { bounds = value; } }

        public Control Parent
        {
            get { return parent; }
            set
            {
                parent = value;
                position.X = parent.X + offset.X;
                position.Y = parent.Y + offset.Y;
                bounds = new Rectangle((int)position.X, (int)position.Y, (int)size.X, (int)size.Y);
            }
        }

        public Control()
        {
            visible = true;
            enabled = true;
            focused = false;
        }

        public Control(string name_, Vector2 size_)
        {
            name = name_;

            position = Vector2.Zero;
            size = size_;

            visible = false;
            enabled = false;
            focused = false;

            bounds = new Rectangle((int)position.X, (int)position.Y, (int)size.X, (int)size.Y);
        }

        public Control(string name_, Vector2 offset_, Vector2 size_)
        {
            name = name_;

            offset = offset_;

            position.X += offset_.X;
            position.Y += offset_.Y;

            size = size_;

            visible = false;
            enabled = false;
            focused = false;

            bounds = new Rectangle((int)position.X, (int)position.Y, (int)size.X, (int)size.Y);
        }

        public virtual void Update(GameTime gameTime) { }

        public virtual void Draw(SpriteBatch spriteBatch) { }

        public void ControlClicked()
        {
            if (clickedEvent != null)
            {
                clickedEvent();
            }
        }
    }
    public delegate void ClickedEventHandler();
    public delegate void UnFocusEventHandler();
    public delegate void FocusEventHandler();
}
