﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using TiledGame.Utils;

namespace TiledGame.GUI
{
    class GUI
    {
        private Dictionary<string, Window> windows;
        private MouseState mouseState;
        private MouseState previousMouseState;

        public Dictionary<string, Window> Windows { get { return windows; } set { windows = value; } }
        public MouseState MouseState { get { return mouseState; } set { mouseState = value; } }

        public GUI()
        {
            windows = new Dictionary<string, Window>();
        }

        public void Update(GameTime gameTime)
        {
            mouseState = Mouse.GetState();

            foreach (KeyValuePair<string, Window> window in windows)
            {
                if (window.Value.Visible && window.Value.Enabled)
                {
                    window.Value.Update(gameTime, mouseState, previousMouseState);
                }
            }

            previousMouseState = mouseState;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (KeyValuePair<string, Window> window in windows)
            {
                if (window.Value.Visible)
                {
                    window.Value.Draw(spriteBatch);
                }
            }
        }
    }
}
