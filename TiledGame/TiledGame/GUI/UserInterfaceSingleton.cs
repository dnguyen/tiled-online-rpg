﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using TiledGame.Utils;

namespace TiledGame.GUI
{
    class UserInterfaceSingleton
    {
        private static UserInterfaceSingleton instance;
        public static UserInterfaceSingleton Instance { get { return instance; } set { instance = value; } }

        private Dictionary<string, Window> windowCache;

        public Dictionary<string, Window> WindowCache { get { return windowCache; } set { windowCache = value; } }
        private GraphicsDevice graphicsDevice;

        public UserInterfaceSingleton(GraphicsDevice graphicsDevice_)
        {
            windowCache = new Dictionary<string, Window>();
            graphicsDevice = graphicsDevice_;
        }

        public Window GetWindow(string name)
        {
            if (!windowCache.ContainsKey(name))
            {
                foreach (string windowXml in Directory.GetFiles("Data\\Interface"))
                {
                    string windowFileName = Path.GetFileName(windowXml.Remove(windowXml.Length - 4));
                    if (name == windowFileName)
                    {
                        XmlDocument xml = new XmlDocument();
                        xml.Load(windowXml);

                        string windowName = xml.GetElementsByTagName("name")[0].InnerText;

                        XmlNode windowPositionNode = xml.GetElementsByTagName("position")[0];
                        float posX = float.Parse(windowPositionNode.Attributes["x"].InnerText);
                        float posY = float.Parse(windowPositionNode.Attributes["y"].InnerText);
                        string backgroundFileName = xml.GetElementsByTagName("background")[0].InnerText;

                        // Load texture using a stream since this won't be on Xbox or WP7.
                        Texture2D background;
                        using (Stream stream = File.OpenRead("Data\\Interface\\Images\\" + backgroundFileName))
                        {
                            background = Texture2D.FromStream(graphicsDevice, stream);
                        }
                        bool visible = bool.Parse(xml.GetElementsByTagName("visible")[0].InnerText);
                        bool enabled = bool.Parse(xml.GetElementsByTagName("enabled")[0].InnerText);
                        bool focused = bool.Parse(xml.GetElementsByTagName("focused")[0].InnerText);
                        bool allowDrag = bool.Parse(xml.GetElementsByTagName("allowDrag")[0].InnerText);

                        Window newWindow = new Window(windowName, new Vector2(posX, posY), new Vector2(background.Width, background.Height))
                        {
                            Texture = background,
                            Visible = visible,
                            Enabled = enabled,
                            Focused = focused,
                            EnableDragging = allowDrag
                        };
                        
                        XmlNode controlsNode = xml.GetElementsByTagName("controls")[0];
                        foreach (XmlNode controlNode in controlsNode.ChildNodes)
                        {
                            string controlName = controlNode.ChildNodes[0].InnerText;

                            // Very restrictive...need to find a different way of doing this
                            float offsetX = float.Parse(controlNode.ChildNodes[1].Attributes["x"].InnerText);
                            float offsetY = float.Parse(controlNode.ChildNodes[1].Attributes["y"].InnerText);

                            // Tried using reflection and a builder dictionary instead, but it didn't work out.
                            switch (controlNode.Name)
                            {
                                case "Label":
                                    SpriteFont font = ResourceManager.Fonts[controlNode.ChildNodes[2].InnerText];
                                    string text = controlNode.ChildNodes[5].InnerText;
                                    Label label = new Label(text, controlName, new Vector2(offsetX, offsetY), Vector2.Zero);
                                    label.Parent = newWindow;
                                    label.Color_ = Color.Black;
                                    label.Visible = true;
                                    label.Enabled = true;
                                    newWindow.AddControl(label);
                                    break;
                            }
                        }

                        return newWindow;
                    }
                }
            }
            return windowCache[name];
        }
    }
}
