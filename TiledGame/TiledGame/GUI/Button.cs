﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace TiledGame.GUI
{
    class Button : Control
    {
        private Texture2D background;

        public Texture2D Background { get { return background; } set { background = value; } }

        public Button(string name, Vector2 offset, Vector2 size)
            : base(name, offset, size)
        {

        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(background, Position, Color.White);
        }
    }

}
