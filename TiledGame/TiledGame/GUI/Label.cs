﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TiledGame.GUI
{
    class Label : Control
    {
        private string text;
        private SpriteFont font;
        private Color color;

        public string Text { get { return text; } set { text = value; } }
        public SpriteFont Font { get { return font; } set { font = value; } }
        public Color Color_ { get { return color; } set { color = value; } }

        public Label() : base()
        {
        }

        public Label(string text_, string name, Vector2 offset, Vector2 size)
            : base(name, offset, size)
        {
            text = text_;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (font != null && color != null)
            {
                spriteBatch.DrawString(font, text, Position, color);
            }
        }

    }
}
