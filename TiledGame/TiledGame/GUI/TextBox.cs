﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using TiledGame.Utils;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace TiledGame.GUI
{
    public delegate void SubmitTextHandler(SubmitTextBoxEventArgs e);
    public class SubmitTextBoxEventArgs {
        private string text;
        public string Text { get { return text; } set { text= value; } }
        public SubmitTextBoxEventArgs(string text_) {
            text= text_;
        }
    }

    class TextBox : Control
    {
        private string text;
        private SpriteFont font;
        private EventInput input;
        private GameWindow window;
        private KeyboardState keyState;
        private KeyboardState oldKeyState;

        public string Text { get { return text; } set { text = value; } }
        public SpriteFont Font { get { return font; } set { font = value; } }
        
        public event SubmitTextHandler SubmitTextEvent;

        public TextBox(string name, Vector2 offset, Vector2 size, GameWindow window_) 
            : base(name, offset, size)
        {
            window = window_;
            input = new EventInput();
            input.Initialize(window);

            input.CharEntered += new TiledGame.Utils.CharEnteredHandler(CharacterEntered);
            text = "";
        }

        public override void Update(GameTime gameTime)
        {
            if (Focused)
            {
                oldKeyState = keyState;
                keyState = Keyboard.GetState();

                if (keyState.IsKeyDown(Keys.Enter) && oldKeyState.IsKeyUp(Keys.Enter))
                {
                    if (SubmitTextEvent != null)
                    {
                        SubmitTextEvent(new SubmitTextBoxEventArgs(text));
                    }
                    text = "";
                }

                oldKeyState = keyState;
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {

            spriteBatch.Draw(ResourceManager.GuiElements["debugpixel"], new Rectangle(Bounds.Left, Bounds.Top, 1, Bounds.Height), Color.Black);
            spriteBatch.Draw(ResourceManager.GuiElements["debugpixel"], new Rectangle(Bounds.Right, Bounds.Top, 1, Bounds.Height), Color.Black);
            spriteBatch.Draw(ResourceManager.GuiElements["debugpixel"], new Rectangle(Bounds.Left, Bounds.Top, Bounds.Width, 1), Color.Black);
            spriteBatch.Draw(ResourceManager.GuiElements["debugpixel"], new Rectangle(Bounds.Left, Bounds.Bottom, Bounds.Width, 1), Color.Black);

            spriteBatch.DrawString(font, text, Position + new Vector2(3,3), Color.Black);
        }

        private void CharacterEntered(object sender, TiledGame.Utils.CharacterEventArgs e)
        {
            if (Focused)
            {
                // Add key to text buffer. If not a symbol key. 
                if (!((int)e.Character < 32 || (int)e.Character > 126)) //From space to tilde
                {
                    // Capitals are already supported in DLL so we don't have to worry about checking shift
                    if (!(Keyboard.GetState().IsKeyDown(Keys.LeftControl) || Keyboard.GetState().IsKeyDown(Keys.RightControl)))
                    {
                        text += e.Character;
                    }
                }

                // Backspace - remove character if there are any
                if ((int)e.Character == 0x08 && text.Length > 0)
                {
                    text = text.Remove(text.Length - 1);
                }
           }
        }
    }
}
