﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;
using TiledGame.Entities.Items;
using TiledGame.Utils;

namespace TiledGame.Handlers
{
    class PlayerEquipItemHandler : PacketHandler
    {
        public void HandlePacket(NetIncomingMessage packet, GameClient client)
        {
            Console.WriteLine("Received packet to eqiup item");

            int equipid = packet.ReadInt32();
            int equiptype = packet.ReadInt32();

            Equipment equip = new Equipment(equipid, ItemDataProvider.Instance.GetEquipAnimations(equipid));
            equip.EquipType = (EquipmentType)equiptype;

            client.Player.EquipItem(equip);
        }
    }
}
