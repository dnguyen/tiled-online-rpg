﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;
using TiledGame.Entities;
using TiledGame.Utils;
using Microsoft.Xna.Framework;
using TiledGame.Engine.GameMap;

namespace TiledGame.Handlers
{
    class SpawnEnemyHandler : PacketHandler
    {
        public void HandlePacket(NetIncomingMessage packet, GameClient client)
        {
            int enemyid = packet.ReadInt32();
            int objectid = packet.ReadInt32();
            EnemyDirection direction = (EnemyDirection)packet.ReadInt32();
            int x = packet.ReadInt32();
            int y = packet.ReadInt32();

            Enemy enemy = new Enemy(enemyid, EnemyDataProvider.Instance.GetEnemyAnimations(enemyid), direction, new Vector2(x, y));
            enemy.ObjectId = objectid;

            client.Player.CurrentMap.Enemies.Add(objectid, enemy);

            Console.WriteLine("**Spawned enemy (" + objectid + ")" + enemy.Id + " in " + client.Player.Name);
            foreach (KeyValuePair<int, Enemy> mapobject in client.Player.CurrentMap.Enemies)
            {
                Console.WriteLine(mapobject.Value.ObjectId);
            }
        }
    }
}
