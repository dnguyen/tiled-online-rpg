﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;

namespace TiledGame.Handlers
{
    interface PacketHandler
    {
        void HandlePacket(NetIncomingMessage packet, GameClient client);
    }
}
