﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;
using TiledGame.Utils;
using TiledGame.Entities;
using Microsoft.Xna.Framework;

namespace TiledGame.Handlers
{
    class RangedAttackHandler : PacketHandler
    {
        public void HandlePacket(NetIncomingMessage packet, GameClient client)
        {
            int charid = packet.ReadInt32();

            Vector2 directionVector = Vector2.Zero;
            switch (client.Player.CurrentMap.Players[charid].Direction)
            {
                case CharacterDirection.UP:
                    directionVector = new Vector2(0, -1);
                    break;
                case CharacterDirection.DOWN:
                    directionVector = new Vector2(0, 1);
                    break;
                case CharacterDirection.LEFT:
                    directionVector = new Vector2(-1, 0);
                    break;
                case CharacterDirection.RIGHT:
                    directionVector = new Vector2(1, 0);
                    break;
            }

            client.Player.CurrentMap.Players[charid].Projectiles.Add(
                new Projectile(
                    ResourceManager.debugTextures["test_projectile"],
                    client.Player.CurrentMap.Players[charid].Position,
                    directionVector));
            
            Console.WriteLine("Trying to ranged attack");
        }
    }
}
