﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;
using TiledGame.Entities;
using TiledGame.Utils;
using Microsoft.Xna.Framework;

namespace TiledGame.Handlers
{
    /// <summary>
    /// Handler for when another player is spawning in the client's map.
    /// </summary>
    class SpawnPlayerMapObjectHandler : PacketHandler
    {
        public void HandlePacket(NetIncomingMessage packet, GameClient c)
        {
            int playerid = packet.ReadInt32();
            string playerName = packet.ReadString();
            CharacterGender gender = (CharacterGender)packet.ReadInt32();
            ClassType class_ = (ClassType)packet.ReadInt32();
            int spawnX = packet.ReadInt32();
            int spawnY = packet.ReadInt32();
            int currentMap = packet.ReadInt32();
            Console.WriteLine("2Server sent spawn other player packet: " + playerid + " " + playerName + " (" + spawnX + "," + spawnY);

            Character newCharacter = new Character(playerid, playerName, ResourceManager.CharacterAnimations[gender.ToString()], new Vector2(spawnX, spawnY), CharacterDirection.DOWN, c);
            newCharacter.Class = class_;
            newCharacter.Gender = gender;
            newCharacter.CurrentMap = ResourceManager.Maps[currentMap];
            newCharacter.CurrentMap.Players.Add(newCharacter.Id, newCharacter);
        }
    }
}
