﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;
using TiledGame.Entities;
using TiledGame.Utils;
using Microsoft.Xna.Framework;
using TiledGame.Entities.Items;

namespace TiledGame.Handlers
{
    /// <summary>
    /// Handler for spawning the client's player.
    /// </summary>
    class SpawnPlayerHandler : PacketHandler
    {
        public void HandlePacket(NetIncomingMessage packet, GameClient client)
        {
            int charid = packet.ReadInt32();
            string playerName = packet.ReadString();
            CharacterGender gender = (CharacterGender)packet.ReadInt32();
            ClassType class_ = (ClassType)packet.ReadInt32();
            int spawnX = packet.ReadInt32();
            int spawnY = packet.ReadInt32();
            int currentMapId = packet.ReadInt32();

            Console.WriteLine("1Server sent spawn player packet: " + playerName + " (" + spawnX + "," + spawnY + " " + currentMapId);
            Console.WriteLine("Class: " + class_);
            client.Player = new Character(charid, playerName, ResourceManager.CharacterAnimations[gender.ToString()], new Vector2(spawnX, spawnY), CharacterDirection.DOWN, client);

            client.Player.Class = class_;
            client.Player.Gender = gender;

            int inventoryRegularItemCount = packet.ReadInt32();
            if (inventoryRegularItemCount > 0)
            {
                for (int i = 0; i < inventoryRegularItemCount; i++)
                {
                    int itemid = packet.ReadInt32();
                    byte itemtype = packet.ReadByte();
                    RegularItem item = new RegularItem(itemid, ItemDataProvider.Instance.GetItemById(itemid).Sprite, 0);
                    client.Player.RegularItems.Items.Add(i, item);
                }
            }

            client.Player.CurrentMap = ResourceManager.Maps[currentMapId];
            client.Player.CurrentMap.Players.Add(client.Player.Id, client.Player);
        }
    }
}
