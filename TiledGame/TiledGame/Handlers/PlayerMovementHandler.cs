﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;
using TiledGame.Entities;

namespace TiledGame.Handlers
{
    class PlayerMovementHandler : PacketHandler
    {
        public void HandlePacket(NetIncomingMessage packet, GameClient c)
        {
            int charid = packet.ReadInt32();
            int posX = packet.ReadInt32();
            int posY = packet.ReadInt32();
            int direction = packet.ReadInt32();

            Character player = c.Player.CurrentMap.GetPlayerById(charid);

            player.X = posX;
            player.Y = posY;
            player.Direction = (CharacterDirection)direction;

            //Console.WriteLine(charid + " moving to (" + posX + "," + posY + ")");
            c.Player.CurrentMap.MovePlayer(player);
        }
    }
}
