﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;
using Microsoft.Xna.Framework;

namespace TiledGame.Handlers
{
    class PlayerChatHandler : PacketHandler
    {
        public event PlayerTalkedHandler PlayerTalkedEvent;

        public void HandlePacket(NetIncomingMessage packet, GameClient client)
        {
            int charid = packet.ReadInt32();
            int type = packet.ReadInt32();
            string text = packet.ReadString();

            switch (type)
            {
                case 0:
                    if (PlayerTalkedEvent != null)
                    {
                        PlayerTalkedEvent(new PlayerTalkedEventArgs(charid, text));
                    }
                    break;
            }

        }
    }
    public delegate void PlayerTalkedHandler(PlayerTalkedEventArgs e);
    public class PlayerTalkedEventArgs : EventArgs {
        private int id;
        private string text;

        public int Id { get { return id; } }
        public string Text { get { return text; } }

        public PlayerTalkedEventArgs(int id_, string text_)
        {
            id = id_;
            text = text_;
        }
    }
}
