﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;

namespace TiledGame.Handlers
{
    class KillEnemyHandler : PacketHandler
    {
        public void HandlePacket(NetIncomingMessage packet, GameClient client)
        {
            int enemyObjectId = packet.ReadInt32();

            if (client.Player.CurrentMap.Enemies.ContainsKey(enemyObjectId))
            {
                client.Player.CurrentMap.Enemies.Remove(enemyObjectId);
                Console.WriteLine("Enemy killed " + enemyObjectId);
            }
            else
            {
                Console.WriteLine("No enemy with key: " + enemyObjectId);
            }
        }
    }
}
