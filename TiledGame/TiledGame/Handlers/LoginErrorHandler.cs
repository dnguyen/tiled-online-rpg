﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;

namespace TiledGame.Handlers
{
    class LoginErrorHandler : PacketHandler
    {
        public void HandlePacket(NetIncomingMessage packet, GameClient client)
        {
            int errorType = packet.ReadInt32();
            Console.WriteLine("Error id: " + errorType);
        }
    }
}
