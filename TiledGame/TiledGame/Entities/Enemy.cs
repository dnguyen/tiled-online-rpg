﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TiledGame.Engine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TiledGame.Engine.GameMap;
using TiledGame.Utils;

namespace TiledGame.Entities
{
    public enum EnemyDirection
    {
        LEFT = 0x01,
        RIGHT = 0x02,
        UP = 0x03,
        DOWN = 0x04
    }

    class Enemy : GameMapObject
    {
        private int id;
        private string name;
        private EnemyDirection direction;
        private Dictionary<EnemyDirection, Animation> animations;
        private Rectangle bounds;

        public int Id { get { return id; } set { id = value; } }
        public string Name { get { return name; } set { name = value; } }
        public EnemyDirection Direction { get { return direction; } set { direction = value; } }
        public Dictionary<EnemyDirection, Animation> Animations { get { return animations; } set { animations = value; } }
        public Rectangle Bounds { get { return bounds; } set { bounds = value; } }

        public Enemy(int id_, Dictionary<EnemyDirection, Animation> animations_, EnemyDirection direction_, Vector2 pos)
        {
            id = id_;
            Position = pos;
            animations = animations_;
            direction = direction_;
            bounds = new Rectangle((int)X, (int)Y, animations[direction_].Sheet.FrameWidth, animations[direction_].Sheet.FrameHeight);
        }

        public void Update(GameTime gameTime)
        {
            bounds = new Rectangle((int)X, (int)Y, animations[direction].Sheet.FrameWidth, animations[direction].Sheet.FrameHeight);
            animations[direction].Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            animations[direction].Draw(spriteBatch, Position);
            spriteBatch.DrawString(ResourceManager.Fonts["CharacterNameDisplay"], ObjectId.ToString(), Position,Color.CornflowerBlue);
        }
    }
}
