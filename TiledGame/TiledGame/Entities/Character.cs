﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TiledGame.Engine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TiledGame.Utils;
using Lidgren.Network;
using TiledGame.Network;
using TiledGame.Entities.Items;
using TiledGame.Engine.GameMap;

namespace TiledGame.Entities
{
    public enum CharacterDirection
    {
        LEFT = 0x01,
        RIGHT = 0x02,
        UP = 0x03,
        DOWN = 0x04
    }

    public enum CharacterGender
    {
        MALE = 0,
        FEMALE = 1
    }

    public enum AttackType 
    {
        MELEE = 0,
        RANGED = 1
    }

    public enum ClassType
    {
        BEGINNER = 100,
        WARRIOR = 200,
        MAGICIAN = 300,
        THIEF = 400,
        BOWMAN = 500
    }

    class Character : GameMapObject
    {
        private int id;
        private GameClient client;
        private string name;
        private CharacterGender gender;
        private ClassType class_;
        private Vector2 velocity;
        private Rectangle bounds;
        private CharacterDirection direction;
        private Dictionary<CharacterDirection, Animation> animations;
        private bool isMoving = false;
        private GameMap currentMap;
        private string chatText;
        private Inventory regularItems;
        private EquippedItemsInventory equippedItems;
        private float chatTimer = 0;

        private List<Projectile> projectiles;

        #region Public Properties
        public int Id { get { return id; } set { id = value; } }
        public GameClient Client { get { return client; } set { client = value; } }
        public Vector2 Velocity { get { return velocity; } set { velocity = value; } }
        public bool IsMoving { get { return isMoving; } set { isMoving = value; } }
        public float VX { get { return velocity.X; } set { velocity.X = value; } }
        public float VY { get { return velocity.Y; } set { velocity.Y = value; } }
        public string Name { get { return name; } set { name = value; } }
        public CharacterGender Gender { get { return gender; } set { gender = value; } }
        public ClassType Class { get { return class_; } set { class_ = value; } }
        public CharacterDirection Direction { get { return direction; } set { direction = value; } }
        public Dictionary<CharacterDirection, Animation> Animations { get { return animations; } set { animations = value; } }
        public GameMap CurrentMap { get { return currentMap; } set { currentMap = value; } }
        public string ChatText { get { return chatText; } set { chatText = value; } }
        public Inventory RegularItems { get { return regularItems; } set { regularItems = value; } }
        public EquippedItemsInventory EquippedItems { get { return equippedItems; } set { equippedItems = value; } }

        public List<Projectile> Projectiles { get { return projectiles; } set { projectiles = value; } }
        #endregion

        public Character(int id_, string name_, Dictionary<CharacterDirection, Animation> animations_, Vector2 position_, CharacterDirection direction_, GameClient client_)
        {
            id = id_;
            client = client_;
            name = name_;
            animations = animations_;
            Position = position_;
            velocity = Vector2.Zero;
            direction = direction_;
            bounds = new Rectangle((int)X, (int)Y, animations[direction].Sheet.FrameWidth, animations[direction].Sheet.FrameHeight);
            chatText = "";
            equippedItems = new EquippedItemsInventory();
            regularItems = new Inventory();
            projectiles = new List<Projectile>();
        }

        public void Update(GameTime gameTime)
        {

            animations[direction].Update(gameTime);

            if (chatText != "")
            {
                chatTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                if (chatTimer >= 5000)
                {
                    chatText = "";
                    chatTimer = 0;
                }
            }

            if (projectiles.Count > 0)
            {
                foreach (Projectile projectile in projectiles)
                {
                    if (projectile.Alive)
                    {
                        projectile.Update(gameTime);

                        // Check for collision with enemies
                        foreach (KeyValuePair<int, Enemy> enemy in currentMap.Enemies)
                        {
                            if (projectile.Bounds.Intersects(enemy.Value.Bounds))
                            {
                                projectile.Alive = false;
                                client.netClient.SendMessage(PacketCreator.EnemyTakeDamage(client, enemy.Value), NetDeliveryMethod.ReliableOrdered);
                                Console.WriteLine("Enemy " + enemy.Value.ObjectId + " taking damage");
                            }
                        }
                    }
                }
            }

            if (equippedItems.Equips.Count > 0)
            {
                foreach (KeyValuePair<int, Equipment> equip in equippedItems.Equips)
                {
                    equip.Value.Animations[(EquipmentDirection)direction].Update(gameTime);
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            animations[direction].Draw(spriteBatch, Position);
            if (projectiles.Count > 0)
            {
                foreach (Projectile projectile in projectiles)
                {
                    if (projectile.Alive)
                        projectile.Draw(spriteBatch);
                }
            }

            foreach (KeyValuePair<int, Equipment> equip in equippedItems.Equips)
            {
                equip.Value.Animations[(EquipmentDirection)direction].Draw(spriteBatch, Position);
            }

            spriteBatch.DrawString(ResourceManager.Fonts["CharacterNameDisplay"], Name, new Vector2(X + (animations[direction].Sheet.FrameWidth / 2) - (ResourceManager.Fonts["CharacterNameDisplay"].MeasureString(Name).X / 2), Y + animations[direction].Sheet.FrameHeight), Color.Azure);
            if (chatText != "")
                spriteBatch.DrawString(ResourceManager.Fonts["CharacterNameDisplay"], Name + ": " + chatText, Position - new Vector2(ResourceManager.Fonts["CharacterNameDisplay"].MeasureString(chatText).X / 2, 20), Color.Black);
        }

        public void DrawCharObject(SpriteBatch spriteBatch)
        {
            animations[direction].Draw(spriteBatch, Position);
        }

        public void Move(CharacterDirection dir)
        {
            direction = dir;
            isMoving = true;
            client.netClient.SendMessage(PacketCreator.MovePlayer(client), NetDeliveryMethod.ReliableOrdered);
        }

        // Normal attacks. NOT attack skills
        public void Attack(AttackType aType)
        {
            switch (aType)
            {
                case AttackType.MELEE:
                    client.netClient.SendMessage(PacketCreator.MeleeAttack(client), NetDeliveryMethod.ReliableOrdered);
                    break;
                case AttackType.RANGED:
                    client.netClient.SendMessage(PacketCreator.RangedAttack(client), NetDeliveryMethod.ReliableOrdered);
                    break;
            }
        }

        public void EquipItem(Equipment equip)
        {
            if (equippedItems.Equips.ContainsKey((int)equip.EquipType))
            {
                equippedItems.Equips.Remove((int)equip.EquipType);
            }
            equippedItems.Equips.Add((int)equip.EquipType, equip);

            Console.WriteLine(client.Player.Name + " equipped " + equippedItems.Equips[(int)equip.EquipType].Name);
            Console.WriteLine("Animation count: " + equip.Animations.Count + " " + equip.EquipType.ToString());
        }
    }
}
