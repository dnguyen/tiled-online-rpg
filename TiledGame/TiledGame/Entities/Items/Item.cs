﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TiledGame.Entities.Items
{
    class Item
    {
        private int id;
        private string name;
        private byte type;
        private Vector2 position;

        public int Id { get { return id; } set { id = value; } }
        public string Name { get { return name; } set { name = value; } }
        public byte Type { get { return type; } set { type = value; } }
        public Vector2 Position { get { return position; } set { position = value; } }
        public float X { get { return position.X; } set { position.X = value; } }
        public float Y { get { return position.Y; } set { position.Y = value; } }

        public Item(int itemid)
        {
            id = itemid;
            position = Vector2.Zero;
        }
    }
}
