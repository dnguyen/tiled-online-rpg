﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TiledGame.Engine;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace TiledGame.Entities.Items
{
    public enum EquipmentDirection
    {
        LEFT = 0x01,
        RIGHT = 0x02,
        UP = 0x03,
        DOWN = 0x04,
        DEFAULT = 0x00
    }

    public enum EquipmentType
    {
        WEAPON = 0,
        HAT = 1,
        TOP = 2,
        BOTTOM = 3,
        SHOES = 4
    }

    class Equipment : Item
    {
        private Dictionary<EquipmentDirection, Animation> animations;
        private EquipmentDirection direction;
        private EquipmentType equipType;

        public Dictionary<EquipmentDirection, Animation> Animations { get { return animations; } set { animations = value; } }
        public EquipmentDirection Direction { get { return direction; } set { direction = value; } }
        public EquipmentType EquipType { get { return equipType; } set { equipType = value; } }

        // Constructor for only data, no positions or directions needed
        public Equipment(int itemid, Dictionary<EquipmentDirection, Animation> animations_)
            : base(itemid)
        {
            animations = animations_;
            direction = EquipmentDirection.LEFT;
        }

        // Constructor used for when an equipment needs to be drawn
        public Equipment(int itemid, Dictionary<EquipmentDirection, Animation> animations_, EquipmentDirection direction_, Vector2 position_)
            : base(itemid)
        {
            animations = animations_;
            direction = direction_;
            Position = position_;
            CalculatePosition();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            animations[direction].Draw(spriteBatch, Position);
        }

        private void CalculatePosition()
        {
            switch (direction)
            {
                case EquipmentDirection.UP:
                    X += (animations[direction].Sheet.FrameWidth) / 2;
                    Y -= animations[direction].Sheet.FrameHeight;
                    break;
                case EquipmentDirection.DOWN:
                    X += (animations[direction].Sheet.FrameWidth) / 2;
                    Y += 64;
                    break;
                case EquipmentDirection.LEFT:
                    X -= animations[direction].Sheet.FrameWidth;
                    Y += (animations[direction].Sheet.FrameHeight) / 2;
                    break;
                case EquipmentDirection.RIGHT:
                    X += 64;
                    Y += (animations[direction].Sheet.FrameHeight);
                    break;
            }
        }
    }
}
