﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace TiledGame.Entities.Items
{
    class RegularItem : Item
    {
        private Texture2D sprite;
        private int quantity;

        public Texture2D Sprite { get { return sprite; } set { sprite = value; } }
        public int Quantity { get { return quantity; } set { quantity = value; } }

        public RegularItem(int itemid, Texture2D sprite_, int quantity_)
            : base(itemid)
        {
            sprite = sprite_;
            quantity = quantity_;
        }
    }
}
