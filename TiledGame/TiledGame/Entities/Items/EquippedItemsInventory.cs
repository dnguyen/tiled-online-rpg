﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiledGame.Entities.Items
{
    class EquippedItemsInventory
    {
        private Dictionary<int, Equipment> equips;
        public Dictionary<int, Equipment> Equips { get { return equips; } set { equips = value; } }

        public EquippedItemsInventory()
        {
            equips = new Dictionary<int, Equipment>();
        }
    }
}
