﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace TiledGame.Entities
{
    class Projectile : Entity
    {
        private bool isActive;
        private Vector2 direction;
        private Vector2 velocity;
        private int speed = 100;
        private bool alive;

        public bool IsActive { get { return isActive; } set { isActive = value; } }
        public Vector2 Velocity { get { return velocity; } set { velocity = value; } }
        public float VX { get { return velocity.X; } set { velocity.X = value; } }
        public float VY { get { return velocity.Y; } set { velocity.Y = value; } }
        public bool Alive { get { return alive; } set { alive = value; } }
        public int Speed { get { return speed; } set { speed = value; } }

        public Projectile(Texture2D sprite, Vector2 pos, Vector2 direction_)
            : base(sprite, pos)
        {
            isActive = false;
            direction = direction_;
            velocity = direction_ * speed;
            alive = true;
        }

        public void Update(GameTime gameTime)
        {
            float elapsedTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            Position += velocity * elapsedTime;
            base.Update();
            //if (Position.X < 0)
                //alive = false;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Sprite, Position, Color.White);
        }
    }
}
