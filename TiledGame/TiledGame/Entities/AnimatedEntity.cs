﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using TiledGame.Engine;

namespace TiledGame.Entities
{
    class AnimatedEntity
    {
        private Animation animation;
        private Vector2 position;
        private Rectangle bounds;

        public AnimatedEntity(Animation animation_, Vector2 pos)
        {
            animation = animation_;
            position = pos;
            bounds = new Rectangle((int)position.X, (int)position.Y, animation.Sheet.FrameWidth, animation.Sheet.FrameHeight);
        }

        public void Update()
        {
            bounds.X = (int)position.X;
            bounds.Y = (int)position.Y;
        }
    }
}
