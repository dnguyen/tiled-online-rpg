﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace TiledGame.Entities
{
    // Non-animated game entity
    class Entity
    {
        private Texture2D sprite;
        private Vector2 position;
        private Rectangle bounds;

        public Texture2D Sprite { get { return sprite; } set { sprite = value; } }
        public Vector2 Position { get { return position; } set { position = value; } }
        public Rectangle Bounds { get { return bounds; } set { bounds = value; } }

        public Entity(Texture2D sprite_, Vector2 pos)
        {
            sprite = sprite_;
            position = pos;
            bounds = new Rectangle((int)position.X, (int)position.Y, sprite.Width, sprite.Height);
        }

        public virtual void Update()
        {
            bounds.X = (int)position.X;
            bounds.Y = (int)position.Y;
        }
    }
}
