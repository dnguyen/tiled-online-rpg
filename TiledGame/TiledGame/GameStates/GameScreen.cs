﻿using System;
using System.Collections.Generic;
using Lidgren.Network;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using TiledGame.Components;
using TiledGame.Engine;
using TiledGame.Entities;
using TiledGame.GUI;
using TiledGame.Network;
using TiledGame.Utils;
using TiledGame.Handlers;
using TiledGame.Entities.Items;

namespace TiledGame.GameStates
{
    // Actual playable game screen
    
    class GameScreen : Screen
    {
        private Camera camera;
        private GameClient client;
        private Dictionary<int, PacketHandler> handlers;
        private PlayerInput playerInput;
        private GUI.GUI gui;
        private Queue<string> chatLog;

        TextBox textBox;

        public GameScreen(Game game, SpriteBatch spriteBatch, GameClient client_)
            : base(game, spriteBatch)
        {
            client = client_;
            camera = new Camera();

            handlers = new Dictionary<int, PacketHandler>();
            ResetHandlers();

            gui = new TiledGame.GUI.GUI();
            playerInput = new PlayerInput();
            chatLog = new Queue<string>();

            Window userBarWindow = new Window("userBar", new Vector2(0, 0), new Vector2(ResourceManager.GuiElements["main_ui"].Width, ResourceManager.GuiElements["main_ui"].Height))
            {
                Texture = ResourceManager.GuiElements["main_ui"],
                Visible = true,
                Enabled = true,
                EnableDragging = false
            };

            textBox = new TextBox("textbox1", new Vector2(80, 527), new Vector2(700, 21), game.Window)
            {
                Font = ResourceManager.Fonts["CharacterNameDisplay"],            
                Visible = true,
                Enabled = true,
                Parent = userBarWindow
            };
            textBox.SubmitTextEvent += new SubmitTextHandler(SubmitChatText);
            textBox.unfocusEvent += new UnFocusEventHandler(UnFocusChatText);
            textBox.focusEvent += new FocusEventHandler(FocusChatText);

            userBarWindow.AddControl(textBox);


            gui.Windows.Add("userBar", userBarWindow);

        }

        public void LoadContent(ContentManager Content)
        {
        }

        public override void Update(GameTime gameTime)
        {
            // read messages
            NetIncomingMessage msg;
            while ((msg = client.netClient.ReadMessage()) != null)
            {
                switch (msg.MessageType)
                {
                    case NetIncomingMessageType.DiscoveryResponse:
                        // just connect to first server discovered
                        client.netClient.Connect(msg.SenderEndpoint);
                        break;
                    case NetIncomingMessageType.Data:
                        
                        int packetId = msg.ReadInt32();

                        try
                        {
                            PacketHandler handler = getHandler(packetId);
                            Console.WriteLine("Recieved packet:" + (RecievePacketIds)packetId + " ("+packetId+")");
                            if (handler != null)
                                handler.HandlePacket(msg, client);
                            else
                                Console.WriteLine("Unhandled packet: " + packetId);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Unhandled packet: " + packetId);
                            Console.WriteLine(e.ToString());
                        }
                        break;
                }
            }

            if (client.Player != null)
            {
                playerInput.Update(gameTime, client, gui);

                client.Player.CurrentMap.Update(gameTime);

                camera.MoveCamera(client.Player, ResourceManager.Maps[client.Player.CurrentMap.Id].TiledMap);
            }

            gui.Update(gameTime);

        }

        public override void Draw(GameTime gameTime)
        {
            Matrix cameraMatrix = Matrix.CreateTranslation(-(int)camera.Position.X, -(int)camera.Position.Y, 0);
            spriteBatch.Begin(0, null, null, null, null, null, cameraMatrix);

            Rectangle visibleArea = new Rectangle((int)camera.X, (int)camera.Y, Game1.WINDOW_WIDTH, Game1.WINDOW_HEIGHT);
            if (client.Player != null)
            {
                client.Player.CurrentMap.Draw(spriteBatch, visibleArea);
            }

            spriteBatch.End();

            spriteBatch.Begin();
            gui.Draw(spriteBatch);
            spriteBatch.End();

            spriteBatch.Begin();

            float yOffset = 0;
            foreach (string texts in chatLog)
            {
                spriteBatch.DrawString(ResourceManager.Fonts["ChatText"], texts, new Vector2(15, 420 - yOffset), Color.Black);
                yOffset -= 20;
            }

            spriteBatch.End();
        }

        public PacketHandler getHandler(int id)
        {
            return handlers[id];
        }

        public void ResetHandlers()
        {
            handlers.Clear();

            handlers.Add((int)RecievePacketIds.SPAWN_PLAYER, new SpawnPlayerHandler());
            handlers.Add((int)RecievePacketIds.SPAWN_PLAYER_MAP_OBJECT, new SpawnPlayerMapObjectHandler());
            handlers.Add((int)RecievePacketIds.MOVE_PLAYER, new PlayerMovementHandler());

            PlayerChatHandler playerChatHandler = new PlayerChatHandler();
            playerChatHandler.PlayerTalkedEvent += new PlayerTalkedHandler(PlayerTalked);
            handlers.Add((int)RecievePacketIds.PLAYER_CHAT, playerChatHandler);
            handlers.Add((int)RecievePacketIds.PLAYER_EQUIP_ITEM, new PlayerEquipItemHandler());
            handlers.Add((int)RecievePacketIds.RANGED_ATTACK, new RangedAttackHandler());
            handlers.Add((int)RecievePacketIds.SPAWN_ENEMY, new SpawnEnemyHandler());
            handlers.Add((int)RecievePacketIds.KILL_ENEMY, new KillEnemyHandler());
        }

        private void SubmitChatText(SubmitTextBoxEventArgs e)
        {
            if (e.Text != "")
            {
                client.netClient.SendMessage(PacketCreator.SendChat(client, 0, e.Text), NetDeliveryMethod.ReliableOrdered);
            }
        }

        private void PlayerTalked(PlayerTalkedEventArgs e)
        {
            if (client.Player.CurrentMap.Players.ContainsKey(e.Id))
            {
                if (chatLog.Count >= 5)
                    chatLog.Dequeue();
                client.Player.CurrentMap.Players[e.Id].ChatText = e.Text;
                chatLog.Enqueue(client.Player.CurrentMap.Players[e.Id].Name + ": " + e.Text);
            }
        }

        private void UnFocusChatText()
        {
            client.Talking = false;
        }

        private void FocusChatText()
        {
            client.Talking = true;
        }
    }

}
