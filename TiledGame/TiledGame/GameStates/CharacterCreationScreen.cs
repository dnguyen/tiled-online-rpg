﻿using Lidgren.Network;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TiledGame.GameStates
{
    class CharacterCreationScreen : Screen
    {
        private NetClient client;

        public CharacterCreationScreen(Game game, SpriteBatch spriteBatch, NetClient client_) : base(game, spriteBatch)
        {
            client = client_;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}
