﻿using System;
using Lidgren.Network;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using TiledGame.Utils;
using TiledGame.GUI;

namespace TiledGame.GameStates
{
    class MenuScreen : Screen
    {
        private GameClient client;

        public event AfterLoginHandler afterLoginEvent;
        public event LogInEventHandler loginEvent;
        private GUI.GUI gui;
        MouseState oldMouseState;

        public MenuScreen(Game game, SpriteBatch spriteBatch, GameClient client_)
            : base(game, spriteBatch)
        {
            client = client_;
            gui = new GUI.GUI();

            GUI.Window loginWindow = new GUI.Window("login", new Vector2(Game1.WINDOW_WIDTH / 2 - (ResourceManager.GuiElements["login"].Width / 2), Game1.WINDOW_HEIGHT / 2 - (ResourceManager.GuiElements["login"].Height / 2)), new Vector2(ResourceManager.GuiElements["login"].Width, ResourceManager.GuiElements["login"].Height))
            {
                Enabled = true,
                Visible = true,
                Focused = true,
                EnableDragging = false,
                Texture = ResourceManager.GuiElements["login"]
            };

            TextBox txtUserName = new TextBox("txtUserName", new Vector2(45, 65), new Vector2(365, 25), game.Window)
            {
                Font = ResourceManager.Fonts["MenuScreenFont"],
                Visible = true,
                Enabled = true,
                Parent = loginWindow
            };

            TextBox txtPassword = new TextBox("txtPassword", new Vector2(45, 145), new Vector2(365, 25), game.Window)
            {
                Font = ResourceManager.Fonts["MenuScreenFont"],
                Visible = true,
                Enabled = true,
                Parent = loginWindow
            };

            Button btnLogin = new Button("btnLogin", new Vector2(34, 200), new Vector2(ResourceManager.GuiElements["login_button"].Width, ResourceManager.GuiElements["login_button"].Height))
            {
                Background = ResourceManager.GuiElements["login_button"],
                Visible = true,
                Enabled = true,
                Parent = loginWindow,
            };
            btnLogin.clickedEvent += new ClickedEventHandler(LoginButtonClicked);

            loginWindow.AddControl(btnLogin);
            loginWindow.AddControl(txtPassword);
            loginWindow.AddControl(txtUserName);
            gui.Windows.Add("login", loginWindow);
        }

        public override void Update(GameTime gameTime)
        {
            if (Enabled)
            {
                MouseState mouseState = Mouse.GetState();
                
                gui.Update(gameTime);

                NetIncomingMessage msg;
                while ((msg = client.netClient.ReadMessage()) != null)
                {
                    switch (msg.MessageType)
                    {
                        case NetIncomingMessageType.Data:
                            int packetId = msg.ReadInt32();
                            if (packetId == 01) // Server allowed client to login to the server
                            {
                                Console.WriteLine("Allowed to login");

                                client.AccountId = msg.ReadInt32();

                                if (afterLoginEvent != null)
                                    afterLoginEvent();
                            }
                            break;
                    }
                }

                oldMouseState = mouseState;

                base.Update(gameTime);
            }
        }

        public override void Draw(GameTime gameTime)
        {
            if (Enabled)
            {
                spriteBatch.Begin();

                gui.Draw(spriteBatch);

                spriteBatch.End();

                base.Draw(gameTime);
            }
        }

        private void LoginButtonClicked()
        {
            Console.WriteLine("event: login button clicked");
            if (loginEvent != null)
            {
                string username = ((TextBox)gui.Windows["login"].Controls["txtUserName"]).Text;
                string password = ((TextBox)gui.Windows["login"].Controls["txtPassword"]).Text;

                loginEvent(new LogInEventArgs(username, password));
            }
        }

    }
    public delegate void LogInEventHandler(LogInEventArgs e);
    public delegate void AfterLoginHandler();

    public class LogInEventArgs : EventArgs {

        public LogInEventArgs(string user, string pw)
        {
            username = user;
            password = pw;
        }

        private string username;
        private string password;

        public string Username { get { return username; } set { username = value; } }
        public string Password { get { return password; } set { password = value; } }
    }
}
