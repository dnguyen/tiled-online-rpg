﻿using System;
using Lidgren.Network;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using TiledGame.Entities;
using TiledGame.GUI;
using TiledGame.Utils;

namespace TiledGame.GameStates
{
    class CharacterSelectionScreen : Screen
    {
        public event SelectedCharacterHandler SelectedCharacterEvent;

        private GameClient client;

        // For multiple character support use a dictionary (Future)
        private Character character;
        private string characterName;
        private GUI.GUI gui;

        public CharacterSelectionScreen(Game game, SpriteBatch spriteBatch, GameClient client_)
            : base(game, spriteBatch)
        {
            client = client_;
            gui = new GUI.GUI();

            Window selectionWindow = new Window("selection", new Vector2(Game1.WINDOW_WIDTH / 2 - (ResourceManager.GuiElements["character_selection"].Width / 2), Game1.WINDOW_HEIGHT / 2 - (ResourceManager.GuiElements["character_selection"].Height / 2)), new Vector2(ResourceManager.GuiElements["character_selection"].Width, ResourceManager.GuiElements["character_selection"].Height));
            selectionWindow.Texture = ResourceManager.GuiElements["character_selection"];
            selectionWindow.Enabled = true;
            selectionWindow.Visible = true;
            selectionWindow.EnableDragging = false;
            
            Button selectCharacterButton = new Button("btnSelectCharacter", new Vector2(157,200), new Vector2(ResourceManager.GuiElements["selectCharacterButton"].Width, ResourceManager.GuiElements["selectCharacterButton"].Height));
            selectCharacterButton.clickedEvent += new ClickedEventHandler(selectCharacterClicked);
            selectCharacterButton.Visible = true;
            selectCharacterButton.Enabled = true;
            selectCharacterButton.Parent = selectionWindow;
            selectCharacterButton.Background = ResourceManager.GuiElements["selectCharacterButton"];
            
            Button newCharacterButton = new Button("btnNewCharacter", new Vector2(157, 240), new Vector2(ResourceManager.GuiElements["newCharacterButton"].Width, ResourceManager.GuiElements["newCharacterButton"].Height));
            newCharacterButton.clickedEvent += new ClickedEventHandler(newCharacterClicked);
            newCharacterButton.Visible = true;
            newCharacterButton.Enabled = true;
            newCharacterButton.Parent = selectionWindow;
            newCharacterButton.Background = ResourceManager.GuiElements["newCharacterButton"];

            selectionWindow.AddControl(selectCharacterButton);
            selectionWindow.AddControl(newCharacterButton);
            gui.Windows.Add("selection", selectionWindow);
        }

        public override void Update(GameTime gameTime)
        {
            if (Enabled)
            {
                NetIncomingMessage msg;
                while ((msg = client.netClient.ReadMessage()) != null)
                {
                    switch (msg.MessageType)
                    {
                        case NetIncomingMessageType.Data:
                            int packetId = msg.ReadInt32();
                            if (packetId == 02) // Show Character packet recieved from server
                            {
                                Console.WriteLine("Show character packet recieved from server");
                                int charid = msg.ReadInt32();
                                characterName = msg.ReadString();
                                string gender = msg.ReadString();
                                character = new Character(charid, characterName, ResourceManager.CharacterAnimations[gender], 
                                    new Vector2(
                                        (Game1.WINDOW_WIDTH / 2 - 16) + 8,
                                        (Game1.WINDOW_HEIGHT / 2 - 16) - 50), 
                                    CharacterDirection.DOWN, 
                                    client);

                            }
                            break;
                    }
                }
                if (character != null)
                    character.Animations[character.Direction].Update(gameTime);

                gui.Update(gameTime);

                base.Update(gameTime);
            }
        }

        public override void Draw(GameTime gameTime)
        {
            if (Enabled)
            {
                spriteBatch.Begin();

                gui.Draw(spriteBatch);

                if (character != null)
                {
                    character.DrawCharObject(spriteBatch);
                }

                spriteBatch.End();

                base.Draw(gameTime);
            }
        }

        private void selectCharacterClicked()
        {
            if (SelectedCharacterEvent != null)
                SelectedCharacterEvent(new SelectedCharacterEventArgs(character.Id));
        }

        private void newCharacterClicked()
        {
            Console.WriteLine("New char button clicked");
        }
    }

    public delegate void SelectedCharacterHandler(SelectedCharacterEventArgs e);
    public class SelectedCharacterEventArgs
    {
        private int id;
        public int Id { get { return id; } set { id = value; } }

        public SelectedCharacterEventArgs(int id_)
        {
            id = id_;
        }
    }
}
