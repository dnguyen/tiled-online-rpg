﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;
using TiledGame.Entities;
using Microsoft.Xna.Framework;

namespace TiledGame
{
    class GameClient
    {
        private NetClient client;
        private int accountid;
        private Character player;
        private bool loggedIn;
        private bool talking;

        public NetClient netClient { get { return client; } set { client = value; } }
        public int AccountId { get { return accountid; } set { accountid = value; } }
        public Character Player { get { return player; } set { player = value; } }
        public bool LoggedIn { get { return loggedIn; } set { loggedIn = value; } }
        public bool Talking { get { return talking; } set { talking = value; } }

        public GameClient()
        {
            talking = false;
        }
    }
}
