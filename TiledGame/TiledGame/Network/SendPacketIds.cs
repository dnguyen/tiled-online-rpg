﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiledGame.Network
{
    public enum SendPacketIds
    {
        PLAYER_LOGIN = 0x01,
        CHARACTER_REQUEST = 0x02,
        REQUEST_SPAWN_PLAYER = 0x03,
        MOVE_PLAYER = 0x04,
        PLAYER_CHAT = 0x05,
        EQUIP_ITEM = 0x06,
        MELEE_ATTACK = 0x07,
        RANGED_ATTACK = 0x08,
        ENEMY_TAKE_DAMAGE = 0x09
    }
}
