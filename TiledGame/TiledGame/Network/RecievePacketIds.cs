﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiledGame.Network
{
    public enum RecievePacketIds
    {
        SPAWN_PLAYER = 0x03,
        MOVE_PLAYER = 0x04,
        PLAYER_CHAT = 0x05,
        PLAYER_EQUIP_ITEM = 0x06,
        MELEE_ATTACK = 0x07,
        RANGED_ATTACK = 0x08,
        SPAWN_ENEMY = 0x09,
        KILL_ENEMY = 0x10,
        SPAWN_PLAYER_MAP_OBJECT = 0x31
    }
}
