﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;
using TiledGame.Entities;

namespace TiledGame.Network
{
    class PacketCreator
    {
        public static NetOutgoingMessage SpawnPlayer(GameClient client, Character player)
        {
            NetOutgoingMessage packet = client.netClient.CreateMessage();
            packet.Write((int)player.Id);
            packet.Write(player.Name);
            return packet;
        }

        public static NetOutgoingMessage RequestLoginAccount(GameClient client, string user, string pw)
        {
            NetOutgoingMessage packet = client.netClient.CreateMessage();

            packet.Write((int)SendPacketIds.PLAYER_LOGIN);
            packet.Write(user);
            packet.Write(pw);

            return packet;
        }

        public static NetOutgoingMessage RequestCharacter(GameClient client)
        {
            NetOutgoingMessage packet = client.netClient.CreateMessage();

            packet.Write((int)SendPacketIds.CHARACTER_REQUEST);
            packet.Write(client.AccountId);

            return packet;
        }

        public static NetOutgoingMessage MovePlayer(GameClient client)
        {
            NetOutgoingMessage packet = client.netClient.CreateMessage();

            packet.Write((int)SendPacketIds.MOVE_PLAYER);
            packet.Write((int)client.Player.Id);
            packet.Write((int)client.Player.Direction);

            return packet;
        }

        public static NetOutgoingMessage RangedAttack(GameClient client)
        {
            NetOutgoingMessage packet = client.netClient.CreateMessage();

            packet.Write((int)SendPacketIds.RANGED_ATTACK);
            packet.Write(client.Player.Id);
            packet.Write((int)client.Player.Direction);

            return packet;
        }

        public static NetOutgoingMessage MeleeAttack(GameClient client)
        {
            NetOutgoingMessage packet = client.netClient.CreateMessage();

            packet.Write((int)SendPacketIds.MELEE_ATTACK);
            packet.Write(client.Player.Id);
            packet.Write((int)client.Player.Direction);

            return packet;
        }

        // Chat types 
        // 0 = All chat
        public static NetOutgoingMessage SendChat(GameClient client, int type, string text)
        {
            NetOutgoingMessage packet = client.netClient.CreateMessage();

            packet.Write((int)SendPacketIds.PLAYER_CHAT);
            packet.Write((int)client.Player.Id);
            packet.Write(type);
            packet.Write(text);

            return packet;
        }

        public static NetOutgoingMessage EquipItem(GameClient client, int equipid)
        {
            NetOutgoingMessage packet = client.netClient.CreateMessage();

            packet.Write((int)SendPacketIds.EQUIP_ITEM);
            packet.Write((int)client.Player.Id);
            packet.Write(equipid);

            return packet;
        }

        public static NetOutgoingMessage EnemyTakeDamage(GameClient client, Enemy enemy)
        {
            NetOutgoingMessage packet = client.netClient.CreateMessage();

            packet.Write((int)SendPacketIds.ENEMY_TAKE_DAMAGE);
            packet.Write(enemy.ObjectId);
           
            return packet;
        }
    }
}
