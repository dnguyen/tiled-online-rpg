﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiledGameServer
{
    // List of ALL the packet ids that are sent to the server by the client.
    public enum RecievePacketIds
    {
        PLAYER_LOGIN = 0x01,
        CHARLIST_REQUEST= 0x02,
        PLAYER_SPAWNED = 0x03,
        PLAYER_MOVED = 0x04,
        PLAYER_CHAT = 0x05,
        PLAYER_EQUIP_ITEM = 0x06,
        MELEE_ATTACK = 0x07,
        RANGED_ATTACK = 0x08,
        ENEMY_TAKE_DAMAGE = 0x09
    }
}
