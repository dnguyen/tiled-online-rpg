﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;
using TiledGameServer.Client;

namespace TiledGameServer.PacketHandlers
{
    interface PacketHandler
    {
        void HandlePacket(NetIncomingMessage packet, GameClient client);
    }
}
