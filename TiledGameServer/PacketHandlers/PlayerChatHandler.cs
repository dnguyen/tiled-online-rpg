﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;
using TiledGameServer.Client;

namespace TiledGameServer.PacketHandlers
{
    class PlayerChatHandler : PacketHandler
    {
        public void HandlePacket(NetIncomingMessage packet, GameClient client)
        {
            int charid = packet.ReadInt32();
            int type = packet.ReadInt32();
            string text = packet.ReadString();

            // Display to all players in map.
            switch (type)
            {
                case 0: // All chat
                    foreach (KeyValuePair<int, Character> player in GameServer.Instance.players[charid].currentMap.Players)
                    {
                        if (text != "")
                            GameServer.Instance.netServer.SendMessage(PacketCreator.SendChat(GameServer.Instance.players[charid].client, type, text), player.Value.client.Session, NetDeliveryMethod.ReliableOrdered); 
                    }
                    break;
            }
        }
    }
}
