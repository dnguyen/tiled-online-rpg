﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;
using TiledGameServer.Client;
using TiledGameServer.Server.Maps;
using TiledGameServer.Tools;
using TiledGameServer.Server.Maps.TiledMapLoading;

namespace TiledGameServer.PacketHandlers
{
    class PlayerMovementHandler : PacketHandler
    {
        public void HandlePacket(NetIncomingMessage packet, GameClient c)
        {
            int charid = packet.ReadInt32();
            int direction = packet.ReadInt32();

            Character player = GameServer.Instance.players[charid];

            //player.Bounds = new System.Drawing.Rectangle((int)player.X, (int)player.Y, 32, 32);
            player.Direction = (CharacterDirection)direction;

            float newx = player.Position.X;
            float newy = player.Position.Y;

            player.Moving = false;
            player.Collided = false;

            switch ((CharacterDirection)direction)
            {
                case CharacterDirection.LEFT:
                    newx -= 2;
                    player.Moving = true;
                    break;
                case CharacterDirection.RIGHT:
                    newx += 2;
                    player.Moving = true;
                    break;
                case CharacterDirection.UP:
                    newy -= 2;
                    player.Moving = true;
                    break;
                case CharacterDirection.DOWN:
                    newy += 2;
                    player.Moving = true;
                    break;
            }

            Point newPos = new Point((int)newx, (int)newy);

            foreach (Tile tile in player.currentMap.TileMap.Layers["collision"].Data) {
                if (tile.Id != 0)
                {
                    if (CollisionDetection.intersects(newPos.X, newPos.Y, 32, 32, tile.Bounds.X, tile.Bounds.Y, tile.Bounds.Width, tile.Bounds.Height))
                    {
                        player.Collided = true;
                    }
                }
            }

            if (!player.Collided)
            {
                if (player.Moving)
                {
                    player.Position = newPos;
                }
            }
            else
            {
                player.Position = player.Position;
            }

            GameServer.Instance.MapFactory.GetMap(player.currentMap.Name).MovePlayer(player);
            GameServer.Instance.MapFactory.GetMap(player.currentMap.Name).broadcastPacket(PacketCreator.MovePlayer(player.client));
        }
    }
}
