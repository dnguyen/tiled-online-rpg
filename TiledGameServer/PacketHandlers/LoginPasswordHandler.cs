﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;
using TiledGameServer.Client;
using System.Text.RegularExpressions;
using TiledGameServer.Database;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace TiledGameServer.PacketHandlers
{
    class LoginPasswordHandler : PacketHandler
    {
        public void HandlePacket(NetIncomingMessage packet, GameClient client)
        {
            Console.WriteLine("Client trying to connect");
            bool allowLogin = false;
            string username = packet.ReadString();
            string password = packet.ReadString();

            // Sanitize data
            username = username.Replace(Convert.ToChar(0x0).ToString(), "");
            password = password.Replace(Convert.ToChar(0x0).ToString(), "");

            // Check database for right username and password (Should encrypt password with the client before)
            DatabaseConnection.CreateConnection();
            MySqlCommand query = DatabaseConnection.connection.CreateCommand();
            query.CommandText = "SELECT * FROM accounts";
            MySqlDataReader reader = query.ExecuteReader();
            while (reader.Read())
            {
                if (username == reader.GetString("username") && password == reader.GetString("password")) {
                    allowLogin = true;
                    client.AccountId = reader.GetInt32("id");
                }
            }
            DatabaseConnection.CloseConnection();

            if (allowLogin)
            {
                GameServer.Instance.netServer.SendMessage(PacketCreator.LoginAccount(client), client.Session, NetDeliveryMethod.ReliableOrdered);
            }
            else
            {
                Console.WriteLine("Not allowed to login");
                // Send error message packets
                //GameServer.Instance.netServer.SendMessage(PacketCreator.LoginError((int)0x01),client.Session, NetDeliveryMethod.ReliableOrdered) ;
            }

            Console.WriteLine(username + "(" + username.Length + ")-" + password + "(" + password.Length + ")");
        }
    }
}
