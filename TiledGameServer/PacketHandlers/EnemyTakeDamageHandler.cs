﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;
using TiledGameServer.Client;
using TiledGameServer.Server.Life;

namespace TiledGameServer.PacketHandlers
{
    class EnemyTakeDamageHandler : PacketHandler
    {
        public void HandlePacket(NetIncomingMessage packet, GameClient client)
        {
            int enemyObjectId = packet.ReadInt32();

            if (client.Player.currentMap.MapObjects.ContainsKey(enemyObjectId))
            {
                Console.WriteLine("Server recieved take damage -> " + enemyObjectId);

                Console.WriteLine("Kill map object: " + client.Player.currentMap.MapObjects[enemyObjectId].ObjectId);
                //client.Player.currentMap.broadcastPacket(PacketCreator.KillEnemy((Enemy)client.Player.currentMap.MapObjects[enemyObjectId]));
                client.Player.currentMap.broadcastPacket(PacketCreator.KillEnemy(enemyObjectId));
                client.Player.currentMap.MapObjects.Remove(enemyObjectId);
            }
            else
            {
                Console.WriteLine("No enemy with key : " + enemyObjectId);
            }
        }
    }
}
