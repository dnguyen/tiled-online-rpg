﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TiledGameServer.Client;
using Lidgren.Network;
using TiledGameServer.Server.Items;

namespace TiledGameServer.PacketHandlers
{
    class PlayerEquipItemHandler : PacketHandler
    {
        public void HandlePacket(NetIncomingMessage packet, GameClient client)
        {
            int charid = packet.ReadInt32();
            int equipid = packet.ReadInt32();

            if (ItemDataProvider.Instance.GetEquipData(equipid).Type == (byte)ItemType.EQUIPMENT) { 
                GameServer.Instance.players[charid].EquipItem(equipid);
            }
        }
    }
}
