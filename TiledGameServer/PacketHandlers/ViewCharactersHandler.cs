﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;
using TiledGameServer.Client;
using TiledGameServer.Database;
using MySql.Data.MySqlClient;

namespace TiledGameServer.PacketHandlers
{
    class ViewCharactersHandler : PacketHandler
    {
        public void HandlePacket(NetIncomingMessage packet, GameClient client)
        {
            int accountid = packet.ReadInt32();

            // Load character data for this account
            DatabaseConnection.CreateConnection();
            MySqlCommand con = DatabaseConnection.connection.CreateCommand();
            con.CommandText = "SELECT * FROM characters WHERE accountid=" + accountid;
            MySqlDataReader read = con.ExecuteReader();

            while (read.Read())
            {
                Character character = new Character(read.GetString("name"), read.GetInt32("x"), read.GetInt32("y"));
                character.Id = read.GetInt32("id");
                GameServer.Instance.netServer.SendMessage(PacketCreator.ShowCharacter(character), client.Session, NetDeliveryMethod.ReliableOrdered);
            }

            Console.WriteLine(accountid);
        }
    }
}
