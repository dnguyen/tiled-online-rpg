﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;
using TiledGameServer.Client;

namespace TiledGameServer.PacketHandlers
{
    class RangedAttackHandler : PacketHandler
    {
        public void HandlePacket(NetIncomingMessage packet, GameClient client)
        {
            int charid = packet.ReadInt32();
            CharacterDirection direction = (CharacterDirection)packet.ReadInt32();

            Character player = GameServer.Instance.players[charid];

            GameServer.Instance.MapFactory.GetMap(player.currentMap.Name).broadcastPacket(PacketCreator.RangedAttack(player.client));
        }
    }
}
