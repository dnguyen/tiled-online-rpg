﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;
using TiledGameServer.Client;
using TiledGameServer.Server.Maps;
using TiledGameServer.Database;
using MySql.Data.MySqlClient;
using TiledGameServer.Server.Life;

namespace TiledGameServer.PacketHandlers
{
    class PlayerSpawnedHandler : PacketHandler
    {
        // Client requesting to spawn a character
        public void HandlePacket(NetIncomingMessage packet, GameClient client)
        {
            int charid = packet.ReadInt32();

            client.Player = Character.LoadCharacterFromDB(charid, client);

            client.Player.currentMap.AddPlayer(client.Player.Id, client.Player);
            Console.WriteLine("Spawned " + client.Player.Name + " in " + client.Player.currentMap.Name + " - " + client.Player.X +","+client.Player.Y);
            // Send message to client to spawn the player
            GameServer.Instance.netServer.SendMessage(PacketCreator.SpawnPlayer(client.Player), client.Session, NetDeliveryMethod.ReliableOrdered);

            GameServer.Instance.players.Add(client.Player.Id, client.Player);

            // Let's the client know of any other players that are already in the map
            if (client.Player.currentMap.Players.Count > 1)
            {
                foreach (KeyValuePair<int, Character> player in client.Player.currentMap.Players)
                {
                    if (player.Value != client.Player)
                    {
                        GameServer.Instance.netServer.SendMessage(PacketCreator.SpawnPlayerMapObject(player.Value), client.Session, NetDeliveryMethod.ReliableOrdered);
                    }
                }
            }

            Console.WriteLine("Recieved client request to spawn player");
            
            // let all the other players that are in the map know that a player has spawned
            
            foreach (KeyValuePair<int, Character> player in client.Player.currentMap.Players)
            {
                if (player.Value != client.Player)
                {
                    GameServer.Instance.netServer.SendMessage(PacketCreator.SpawnPlayerMapObject(client.Player), player.Value.client.Session, NetDeliveryMethod.ReliableOrdered);
                }
            }


            Enemy testEnemy = EnemyDataProvider.Instance.GetEnemy(1);
            testEnemy.X = 200;
            testEnemy.Y = 200;
            client.Player.currentMap.AddMapObject(testEnemy);
            client.Player.currentMap.SpawnEnemy(testEnemy);

            Enemy testEnemy2 = EnemyDataProvider.Instance.GetEnemy(1);
            testEnemy2.X = 400;
            testEnemy2.Y = 400;
            client.Player.currentMap.AddMapObject(testEnemy2);
            client.Player.currentMap.SpawnEnemy(testEnemy2);

            foreach (KeyValuePair<int, GameMapObject> mapobjects in client.Player.currentMap.MapObjects)
            {
                Console.WriteLine(mapobjects.Value.ObjectId);
            }
        }
    }
}
