﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace TiledGameServer.Database
{
    class DatabaseConnection
    {
        private const string HOST = "localhost";
        private const string DATABASE = "tiledgame";
        private const string USERNAME = "root";
        private const string PASSWORD = "";

        public static MySqlConnection connection;

        public static void CreateConnection()
        {
            connection = new MySqlConnection();
            connection.ConnectionString = "server=" + HOST + ";database=" + DATABASE + ";username=" + USERNAME + ";password=" + PASSWORD + ";";
            try
            {
                connection.Open();
            }
            catch (MySqlException se)
            {
                Console.WriteLine(se.ToString());
            }
        }

        public static void CloseConnection()
        {
            connection.Close();
        }
    }
}
