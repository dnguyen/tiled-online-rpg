﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;
using TiledGameServer.Client;
using TiledGameServer.Server.Items;
using TiledGameServer.Server.Life;

namespace TiledGameServer
{
    // Creates ALL packets that are sent to the client. The client then handles the packets.
    class PacketCreator
    {
        public static void SendCharacterName(NetServer server, string clientId)
        {
            NetOutgoingMessage packet = server.CreateMessage();
            packet.Write(clientId);
        }

        public static NetOutgoingMessage SpawnPlayer(Character player)
        {
            NetOutgoingMessage msg = GameServer.Instance.netServer.CreateMessage();

            msg.Write((int)SendPacketIds.SPAWN_PLAYER);
            msg.Write((int)player.Id);
            msg.Write(player.Name);
            msg.Write((int)player.Gender);
            msg.Write((int)player.Class);
            msg.Write((int)player.X);
            msg.Write((int)player.Y);
            msg.Write((int)player.currentMap.Id);

            // Send inventory data

            msg.Write((int)player.Items.Items.Count);

            if (player.Items.Items.Count > 0)
            {
                foreach (KeyValuePair<int, Item> item in player.Items.Items)
                {
                    msg.Write((int)item.Value.Id);
                    msg.Write(item.Value.Type);
                    Console.WriteLine("Sending item: " + item.Value.Id + "-" + item.Value.Type);
                }
            }

            return msg;
        }

        public static NetOutgoingMessage SpawnPlayerMapObject(Character player)
        {
            NetOutgoingMessage msg = GameServer.Instance.netServer.CreateMessage();

            msg.Write((int)SendPacketIds.SPAWN_PLAYER_MAP_OBJECT);
            msg.Write((int)player.Id);
            msg.Write(player.Name);
            msg.Write((int)player.Gender);
            msg.Write((int)player.Class);
            msg.Write((int)player.X);
            msg.Write((int)player.Y);
            msg.Write((int)player.currentMap.Id);

            // send equipped items data
            return msg;
        }

        public static NetOutgoingMessage LoginAccount(GameClient client)
        {
            NetOutgoingMessage msg = GameServer.Instance.netServer.CreateMessage();

            msg.Write((int)SendPacketIds.PLAYER_LOGIN);
            msg.Write((int)client.AccountId);

            return msg;
        }

       /* public static NetOutgoingMessage LoginError(int loginErrorType)
        {
            NetOutgoingMessage msg = GameServer.Instance.netServer.CreateMessage();

            msg.Write((int)SendPacketIds.LOGIN_ERROR);
            msg.Write(loginErrorType);
            switch (loginErrorType)
            {
                case (int)0x01:
                    // Invalid username or password
                    msg.Write((int)0x01);
                    break;
                case 0x02:
                    // Connection not found
                    break;
            }

            return msg;
        }*/

        public static NetOutgoingMessage ShowCharacter(Character character)
        {
            NetOutgoingMessage msg = GameServer.Instance.netServer.CreateMessage();

            msg.Write((int)SendPacketIds.SHOW_CHARACTER);
            msg.Write((int)character.Id);
            msg.Write(character.Name);
            msg.Write(character.Gender.ToString());

            return msg;
        }

        public static NetOutgoingMessage MovePlayer(GameClient client)
        {
            NetOutgoingMessage msg = GameServer.Instance.netServer.CreateMessage();

            msg.Write((int)SendPacketIds.PLAYER_MOVE);
            msg.Write((int)client.Player.Id);
            msg.Write((int)client.Player.X);
            msg.Write((int)client.Player.Y);
            msg.Write((int)client.Player.Direction);
            Console.WriteLine("Sending player movement: " + client.Player.X + "," + client.Player.Y);
            return msg;
        }

        public static NetOutgoingMessage RangedAttack(GameClient client)
        {
            NetOutgoingMessage packet = GameServer.Instance.netServer.CreateMessage();

            packet.Write((int)SendPacketIds.RANGED_ATTACK);
            packet.Write((int)client.Player.Id);
            packet.Write((int)client.Player.Direction);

            return packet;
        }

        public static NetOutgoingMessage SendChat(GameClient client, int type, string text)
        {
            NetOutgoingMessage msg = GameServer.Instance.netServer.CreateMessage();

            msg.Write((int)SendPacketIds.PLAYER_CHAT);
            msg.Write(client.Player.Id);
            msg.Write(type);
            msg.Write(text);

            return msg;
        }

        public static NetOutgoingMessage PlayerEquipItem(GameClient client, Equipment equip)
        {
            NetOutgoingMessage packet = GameServer.Instance.netServer.CreateMessage();

            packet.Write((int)SendPacketIds.PLAYER_EQUIP_ITEM);
            packet.Write((int)equip.Id);
            packet.Write((int)equip.EquipType);

            return packet;
        }

        public static NetOutgoingMessage SpawnEnemy(Enemy enemy)
        {
            NetOutgoingMessage packet = GameServer.Instance.netServer.CreateMessage();

            packet.Write((int)SendPacketIds.SPAWN_ENEMY);
            packet.Write(enemy.Id);
            packet.Write(enemy.ObjectId);
            packet.Write((int)enemy.Direction);
            packet.Write(enemy.X);
            packet.Write(enemy.Y);

            return packet;
        }

        public static NetOutgoingMessage KillEnemy(Enemy enemy)
        {
            NetOutgoingMessage packet = GameServer.Instance.netServer.CreateMessage();

            packet.Write((int)SendPacketIds.KILL_ENEMY);
            packet.Write(enemy.ObjectId);

            return packet;
        }

        public static NetOutgoingMessage KillEnemy(int objectid)
        {
            NetOutgoingMessage packet = GameServer.Instance.netServer.CreateMessage();

            packet.Write((int)SendPacketIds.KILL_ENEMY);
            packet.Write(objectid);

            return packet;
        }
    }
}
