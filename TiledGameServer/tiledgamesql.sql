# --------------------------------------------------------
# Host:                         127.0.0.1
# Server version:               5.5.8
# Server OS:                    Win32
# HeidiSQL version:             6.0.0.3603
# Date/time:                    2011-09-17 12:42:22
# --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

# Dumping database structure for tiledgame
CREATE DATABASE IF NOT EXISTS `tiledgame` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `tiledgame`;


# Dumping structure for table tiledgame.accounts
CREATE TABLE IF NOT EXISTS `accounts` (
  `id` int(10) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

# Dumping data for table tiledgame.accounts: ~2 rows (approximately)
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` (`id`, `username`, `password`) VALUES
	(1, 'lithica', 'dielawn'),
	(2, 'dnguyen', 'dielawn');
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;


# Dumping structure for table tiledgame.characters
CREATE TABLE IF NOT EXISTS `characters` (
  `id` int(10) NOT NULL,
  `accountid` int(10) NOT NULL,
  `name` text,
  `chartype` text NOT NULL,
  `map` text,
  `x` int(11) DEFAULT NULL,
  `y` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

# Dumping data for table tiledgame.characters: ~2 rows (approximately)
/*!40000 ALTER TABLE `characters` DISABLE KEYS */;
INSERT INTO `characters` (`id`, `accountid`, `name`, `chartype`, `map`, `x`, `y`) VALUES
	(1, 1, 'Lithica', '1', 'map1', 177, 200),
	(2, 2, 'Dylan', '1', 'map1', 100, 100);
/*!40000 ALTER TABLE `characters` ENABLE KEYS */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
