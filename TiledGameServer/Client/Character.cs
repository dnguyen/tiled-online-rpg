﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TiledGameServer.Client;
using TiledGameServer.Server.Maps;
using TiledGameServer.Database;
using MySql.Data.MySqlClient;
using System.Drawing;
using TiledGameServer.Server.Items;

namespace TiledGameServer
{
    public enum CharacterDirection
    {
        LEFT = 0x01,
        RIGHT = 0x02,
        UP = 0x03,
        DOWN = 0x04
    }

    public enum CharacterGender
    {
        MALE = 0,
        FEMALE = 1
    }

    class Character : GameMapObject
    {
        public GameClient client { get; set; }

        private int id;
        public string Name { get; set; }
        private CharacterGender gender;
        private ClassType class_;
        public GameMap currentMap { get; set; }
        public CharacterDirection Direction { get; set; }
        private bool moving;

        private Inventory items; // Regular inventory (Stores ALL items including equipment)
        private Inventory equippedItems; // "Inventory" for only equipped items
        
        public int Id { get { return id; } set { id = value; } }
        public CharacterGender Gender { get { return gender; } set { gender = value; } }
        public ClassType Class { get { return class_; } set { class_ = value; } }
        public Inventory Items { get { return items; } set { items = value; } }
        public Inventory EquippedItems { get { return equippedItems; } set { equippedItems = value; } }
        public bool Moving { get { return moving; } set { moving = value; } }

        public Character()
        {
            Name = "";
            items = new Inventory();
            equippedItems = new Inventory();
            moving = false;
        }

        public Character(string name_, float x, float y)
        {
            Name = name_;
            items = new Inventory();
            equippedItems = new Inventory();
            moving = false;
        }

        public static Character LoadCharacterFromDB(int charid, GameClient client_)
        {
            Character character = new Character();
            DatabaseConnection.CreateConnection();

            MySqlCommand con = DatabaseConnection.connection.CreateCommand();
            con.CommandText = "SELECT * FROM characters WHERE id=" + charid;
            MySqlDataReader read = con.ExecuteReader();

            while (read.Read())
            {
                character.client = client_;
                character.Id = charid;
                character.Name = read.GetString("name");
                character.Gender = (CharacterGender)read.GetInt32("gender");
                character.Class = (ClassType)read.GetInt32("class");
                character.X = read.GetInt32("x");
                character.Y = read.GetInt32("y");
                character.currentMap = GameServer.Instance.MapFactory.GetMap(read.GetString("map"));
            }
            read.Close();

            con = DatabaseConnection.connection.CreateCommand();
            con.CommandText = "SELECT * FROM inventoryitems WHERE characterid=" + charid;
            read = con.ExecuteReader();

            while (read.Read())
            {
                Item item = new Item(int.Parse(read["itemid"].ToString()));
                item.Type = byte.Parse(read["type"].ToString());
                item.Position = byte.Parse(read["position"].ToString());
                Console.WriteLine(read["characterid"].ToString()+"--"+read["itemid"].ToString() + ": " + read["type"].ToString());
                character.Items.Items.Add(item.Position, item);
            }
            read.Close();
            DatabaseConnection.CloseConnection();

            character.ObjectId = character.Id;
            client_.Player = character;

            return character;
        }

        public void Save()
        {
            try
            {
                DatabaseConnection.CreateConnection();
                MySqlCommand con = DatabaseConnection.connection.CreateCommand();
                con.CommandText = "UPDATE characters"
                    + " SET"
                    + " `name` = '" + Name + "'"
                    + ", `gender` = '" +(int)Gender + "'"
                    + ", `class` = '" + (int)Class + "'"
                    + ", `map` = '" + currentMap.Name + "'"
                    + ", `x` = " + X
                    + ", `y` = " + Y
                    + " WHERE `id` = " + id
                    ;
                con.ExecuteNonQuery();
                DatabaseConnection.CloseConnection();

            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public void EquipItem(int equipid)
        {
            Equipment equip = ItemDataProvider.Instance.GetEquipData(equipid);

            // TODO: Check if the item is in the regular inventory, if it is: remove the item from the inventory
            // TODO: Load equipment data from database (might have different stats than the original equip)
            if (equippedItems.Items.ContainsKey((int)equip.Type))
            {
                equippedItems.Items.Remove((int)equip.Type);
            }
            equippedItems.Items.Add(equip.Type, equip);
            // TODO: Item checks. (Level, stat, etc requirements)
            GameServer.Instance.netServer.SendMessage(PacketCreator.PlayerEquipItem(client, equip), client.Session, Lidgren.Network.NetDeliveryMethod.ReliableOrdered);
        }

    }
}
