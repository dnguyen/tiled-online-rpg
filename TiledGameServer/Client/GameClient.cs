﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;

namespace TiledGameServer.Client
{
    class GameClient
    {
        private int accountid;
        private string uniquekey;
        private Character player;
        private bool loggedIn = false;
        private NetConnection session;

        public string UniqueKey { get { return uniquekey; } set { uniquekey = value; } }
        public Character Player { get { return player; } set { player = value; } }
        public bool LoggedIn { get { return loggedIn; } set { loggedIn = value; } }
        public NetConnection Session { get { return session; } set { session = value; } }
        public int AccountId { get { return accountid; } set { accountid = value; } }

        public GameClient(string key, NetConnection session_)
        {
            uniquekey = key;
            session = session_;
        }

        public void Disconnect()
        {
            player.Save();
            player.currentMap.Players.Remove(player.Id);
            GameServer.Instance.players.Remove(player.Id);
            GameServer.Instance.Clients.Remove(uniquekey);
        }
    }
}
