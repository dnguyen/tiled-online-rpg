﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiledGameServer.Client
{
    public enum ClassType
    {
        BEGINNER = 100,
        WARRIOR = 200,
        MAGICIAN = 300,
        THIEF = 400,
        BOWMAN = 500
    }
}
