﻿using System;
using System.Collections.Generic;
using System.Threading;
using Lidgren.Network;
using TiledGameServer.Client;
using TiledGameServer.Database;
using TiledGameServer.PacketHandlers;
using TiledGameServer.Server.Maps;

namespace TiledGameServer
{
    class GameServer
    {
        public const string CONNECTION_NAME = "TiledGameServer";
        public const int PORT = 14242;
        private NetServer server;
        private Dictionary<int, PacketHandler> handlers;
        private string clientKey = "";
        private Dictionary<string, GameClient> clients;
        public Dictionary<int, Character> players;
        private GameMapFactory mapFactory;

        private static GameServer instance;
        public static GameServer Instance { get { return instance; } }
        public NetServer netServer { get { return server; } }
        public GameMapFactory MapFactory { get { return mapFactory; } }
        public Dictionary<string, GameClient> Clients { get { return clients; } }

        public static void newInstance()
        {
            instance = new GameServer();
        }

        public GameServer()
        {
            CreateServerConnection();
            handlers = new Dictionary<int, PacketHandler>();
            ResetHandlers();
            Console.WriteLine("Handlers reset");

            clients = new Dictionary<string, GameClient>();
            players = new Dictionary<int, Character>();
            mapFactory = new GameMapFactory();
            DatabaseConnection.CreateConnection(); // Test database connection
            Console.WriteLine("Database connection made!");
            DatabaseConnection.CloseConnection();
        }

        public void Run()
        {
            // Server Handler
            // ========================
            double nextSendUpdates = NetTime.Now;
            // TODO: Add when server shut down packet is sent by admin, stop loop.
            while (!Console.KeyAvailable || Console.ReadKey().Key != ConsoleKey.Escape)
            {
                NetIncomingMessage packet;
                while ((packet = server.ReadMessage()) != null)
                {
                    switch (packet.MessageType)
                    {
                        case NetIncomingMessageType.ConnectionApproval:
                            Console.WriteLine("Recieved client approval packet: " + packet.ReadString());

                            break;
                        case NetIncomingMessageType.DiscoveryRequest:
                            //
                            // Server received a discovery request from a client; send a discovery response (with no extra data attached)
                            //
                            server.SendDiscoveryResponse(null, packet.SenderEndpoint);
                            break;
                        case NetIncomingMessageType.VerboseDebugMessage:
                        case NetIncomingMessageType.DebugMessage:
                        case NetIncomingMessageType.WarningMessage:
                        case NetIncomingMessageType.ErrorMessage:
                            //
                            // Just print diagnostic messages to console
                            //
                            Console.WriteLine(packet.ReadString());
                            break;
                        case NetIncomingMessageType.StatusChanged:
                            NetConnectionStatus status = (NetConnectionStatus)packet.ReadByte();
                            if (status == NetConnectionStatus.Connected)
                            {
                                //
                                // A new client just connected!
                                //

                                Console.WriteLine(NetUtility.ToHexString(packet.SenderConnection.RemoteUniqueIdentifier) + " connected!");
                                clientKey = NetUtility.ToHexString(packet.SenderConnection.RemoteUniqueIdentifier);
                                GameClient client = new GameClient(clientKey, packet.SenderConnection);
                                client.Session = packet.SenderConnection;
                                clients.Add(clientKey, client); // Create a new gameclient when a client connects
                                Console.WriteLine(packet.ReadInt32());
                            }
                            else if (status == NetConnectionStatus.Disconnected)
                            {
                                Console.WriteLine(clients[clientKey].UniqueKey + " disconnected");
                                clients[clientKey].Disconnect();
                            }
                            break;
                        case NetIncomingMessageType.Data:
                            int packetid = packet.ReadInt32();
                            Console.WriteLine("Packet Header Recieved: " + (RecievePacketIds)packetid + " ("+packetid+") From " + clients[clientKey].UniqueKey);
                            PacketHandler packetHandler = getHandler(packetid);
                            if (packetHandler != null)
                                packetHandler.HandlePacket(packet, clients[clientKey]);
                            else
                                Console.WriteLine("Unhandled packet header: " + packetid);

                            break;
                    }
                    //
                    // Send pings to make sure the client is still connected
                    //
                    double now = NetTime.Now;
                    if (now > nextSendUpdates)
                    {
                        if (server.ConnectionsCount != 0)
                        {

                        }
                    }

                    // schedule next update
                    nextSendUpdates += (1.0 / 30.0);
                }
                
                Thread.Sleep(1);
            }
            server.Shutdown("app exiting");
        }

        public void CreateServerConnection()
        {
            NetPeerConfiguration config = new NetPeerConfiguration(CONNECTION_NAME);
            config.EnableMessageType(NetIncomingMessageType.DiscoveryRequest);
            config.Port = PORT;

            // create and start server
            server = new NetServer(config);
            server.Start();

            Console.WriteLine("Server Started");
        }

        private PacketHandler getHandler(int packetid)
        {
            return handlers[packetid];
        }

        private void ResetHandlers()
        {
            handlers.Clear();

            handlers.Add((int)RecievePacketIds.PLAYER_LOGIN, new LoginPasswordHandler());
            handlers.Add((int)RecievePacketIds.CHARLIST_REQUEST, new ViewCharactersHandler());
            handlers.Add((int)RecievePacketIds.PLAYER_SPAWNED, new PlayerSpawnedHandler());
            handlers.Add((int)RecievePacketIds.PLAYER_MOVED, new PlayerMovementHandler());
            handlers.Add((int)RecievePacketIds.PLAYER_CHAT, new PlayerChatHandler());
            handlers.Add((int)RecievePacketIds.PLAYER_EQUIP_ITEM, new PlayerEquipItemHandler());
            handlers.Add((int)RecievePacketIds.RANGED_ATTACK, new RangedAttackHandler());
            handlers.Add((int)RecievePacketIds.MELEE_ATTACK, new MeleeAttackHandler());
            handlers.Add((int)RecievePacketIds.ENEMY_TAKE_DAMAGE, new EnemyTakeDamageHandler());
        }
    }
}