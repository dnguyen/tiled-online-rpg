﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiledGameServer.Server.Items
{
    class Inventory
    {
        private Dictionary<int, Item> items;

        public Dictionary<int, Item> Items { get { return items; } set { items = value; } }

        public Inventory()
        {
            items = new Dictionary<int, Item>();
        }
    }
}
