﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiledGameServer.Server.Items
{
    public enum EquipmentType
    {
        WEAPON = 0,
        HAT = 1,
        TOP = 2,
        BOTTOM = 3,
        SHOES = 4
    }

    class Equipment : Item
    {
        private EquipmentType equipType;

        public EquipmentType EquipType { get { return equipType; } set { equipType = value; } }

        public Equipment(int itemid)
            : base(itemid)
        {
        }
    }
}
