﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiledGameServer.Server.Items
{
    class RegularItem : Item
    {
        private int quantity;

        public int Quantity { get { return quantity; } set { quantity = value; } }

        public RegularItem(int itemid, int quantity_)
            : base(itemid)
        {
            quantity = quantity_;
        }
    }
}
