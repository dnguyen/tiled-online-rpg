﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;

namespace TiledGameServer.Server.Items
{
    class ItemDataProvider
    {
        public static ItemDataProvider Instance = new ItemDataProvider();

        private Dictionary<int, RegularItem> regularItemCache;
        private Dictionary<int, Equipment> equipmentCache;

        public Dictionary<int, RegularItem> RegularItemCache { get { return regularItemCache; } }
        public Dictionary<int, Equipment> EquipmentCache { get { return equipmentCache; } }

        public ItemDataProvider()
        {
            regularItemCache = new Dictionary<int, RegularItem>();
            equipmentCache = new Dictionary<int, Equipment>();
        }

        public RegularItem GetItemData(int id)
        {
            RegularItem item = null;

            if (!RegularItemCache.ContainsKey(id))
            {
                foreach (string itemxml in Directory.GetFiles("Data\\Items\\Regular"))
                {
                    string itemFileName = Path.GetFileName(itemxml.Remove(itemxml.Length - 4));

                    if (id == int.Parse(itemFileName))
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(itemxml);

                        XmlNode itemName = doc.GetElementsByTagName("name")[0];
                        XmlNode itemType = doc.GetElementsByTagName("type")[0];
                        item = new RegularItem(id, 0);
                        item.Name = itemName.InnerText;
                        item.Type = byte.Parse(itemType.ToString());
                        Console.WriteLine(item.Name + ":" + item.Type);

                        RegularItemCache.Add(item.Id, item);
                        break;
                    }

                }
            }
            else
            {
                item = RegularItemCache[id];
            }

            return item;
        }

        public Equipment GetEquipData(int id)
        {
            Equipment equip = null;
            if (!equipmentCache.ContainsKey(id))
            {
                foreach (string directory in Directory.GetDirectories("Data\\Items\\Equipment"))
                {
                    foreach (string weaponXMLFile in Directory.GetFiles(directory))
                    {
                        string weaponFileName = Path.GetFileName(weaponXMLFile.Remove(weaponXMLFile.Length - 4));

                        if (id == int.Parse(weaponFileName))
                        {
                            XmlDocument doc = new XmlDocument();
                            doc.Load(weaponXMLFile);

                            XmlNode equipName = doc.GetElementsByTagName("name")[0];
                            Console.WriteLine(equipName.InnerText);

                            EquipmentType equipType = (EquipmentType)Enum.Parse(typeof(EquipmentType), doc.GetElementsByTagName("type")[0].InnerText);

                            equip = new Equipment(id);
                            equip.Name = equipName.InnerText;
                            equip.Type = (byte)ItemType.EQUIPMENT;
                            equip.EquipType = equipType;
                            equipmentCache.Add(id, equip);
                            break;
                        }
                    }
                }
            }
            else
            {
                equip = equipmentCache[id];
            }
            return equip;
        }

    }
}
