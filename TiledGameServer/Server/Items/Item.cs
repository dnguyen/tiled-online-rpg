﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiledGameServer.Server.Items
{
    public enum ItemType
    {
        REGULAR = 0,
        USE = 1,
        EQUIPMENT = 2
    }

    class Item
    {
        private int id;
        private string name;
        private byte type;
        private byte position;

        public int Id { get { return id; } set { id = value; } }
        public string Name { get { return name; } set { name = value; } }
        public byte Type { get { return type; } set { type = value; } }
        public byte Position { get { return position; } set { position = value; } }

        public Item(int itemid)
        {
            id = itemid;
        }
    }
}
