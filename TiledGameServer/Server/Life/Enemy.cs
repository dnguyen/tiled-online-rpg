﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TiledGameServer.Server.Maps;

namespace TiledGameServer.Server.Life
{
    public enum EnemyDirection
    {
        LEFT = 0x01,
        RIGHT = 0x02,
        UP = 0x03,
        DOWN = 0x04
    }

    public enum EnemyStatus
    {
        DEAD = -1,
        IDLE = 0,
        WANDER = 1
    }

    class Enemy : GameMapObject
    {
        private int id;
        private string name;
        private EnemyDirection direction;
        private EnemyStatus status;

        public int Id { get { return id; } set { id = value; } }
        public string Name { get { return name; } set { name = value; } }
        public EnemyDirection Direction { get { return direction; } set { direction = value; } }
        public EnemyStatus Status { get { return status; } set { status = value; } }

        public Enemy(int id_)
        {
            id = id_;
            status = EnemyStatus.IDLE;
        }
    }
}
