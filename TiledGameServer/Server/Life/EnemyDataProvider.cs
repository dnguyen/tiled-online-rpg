﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;

namespace TiledGameServer.Server.Life
{
    class EnemyDataProvider
    {
        private static EnemyDataProvider instance;

        public static EnemyDataProvider Instance
        {
            get
            {
                if (instance == null)
                    instance = new EnemyDataProvider();

                return instance;
            }
            set { instance = value; }
        }

        private Dictionary<int, Enemy> enemyCache;
        public Dictionary<int, Enemy> EnemyCache { get { return enemyCache; } set { enemyCache = value; } }

        public EnemyDataProvider()
        {
            enemyCache = new Dictionary<int, Enemy>();
        }

        public Enemy GetEnemy(int id)
        {
            Enemy enemy = null;

            // Make sure the monster isn't already in the dictionary. No need to reload a monster that has already been loaded.
            if (!enemyCache.ContainsKey(id))
            {
                foreach (string enemyXml in Directory.GetFiles("Data\\enemies"))
                {
                    string enemyFileName = Path.GetFileName(enemyXml.Remove(enemyXml.Length - 4));

                    if (id == int.Parse(enemyFileName))
                    {
                        XmlDocument doc = new XmlDocument();
                        doc.Load(enemyXml);

                        string enemyName = doc.GetElementsByTagName("name")[0].InnerText;

                        enemy = new Enemy(id);
                        enemy.Name = enemyName;
                        enemy.Direction = EnemyDirection.LEFT;

                        enemyCache.Add(id, enemy);
                        break;
                    }
                }
            }
            else
            {
                return enemyCache[id];
            }

            return enemy;
        }


    }
}
