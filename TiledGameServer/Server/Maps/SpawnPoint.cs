﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TiledGameServer.Server.Life;
using TiledGameServer.Tools;

namespace TiledGameServer.Server.Maps
{
    class SpawnPoint
    {
        private Enemy enemy;
        private int x, y;
        private int mobTime;
        private long nextSpawn;

        public Enemy Enemy_ { get { return enemy; } set { enemy = value; } }
        public int X { get { return x; } set { x = value; } }
        public int Y { get { return y; } set { y = value; } }

        public SpawnPoint(Enemy enemy_, int mobTime_)
        {
            enemy = enemy_;
            mobTime = mobTime_;
            nextSpawn = TimeTools.CurrentTimeMillis();
        }

        public bool ShouldSpawn()
        {
            if (mobTime < 0)
                return false;
            return nextSpawn <= TimeTools.CurrentTimeMillis();
        }

        public void SpawnEnemy(GameMap map)
        {
            //if (enemy.Status == EnemyStatus.DEAD)
            //{
            Random rand = new Random();
                nextSpawn = TimeTools.CurrentTimeMillis() + mobTime * 1000;
                enemy.X = rand.Next(500);
                enemy.Y = rand.Next(500);
                map.SpawnEnemy(enemy);
           //}
        }

    }
}
