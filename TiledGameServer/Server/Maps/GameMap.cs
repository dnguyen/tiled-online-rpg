﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;
using TiledGameServer.Server.Life;
using System.Timers;
using TiledGameServer.Server.Maps.TiledMapLoading;

namespace TiledGameServer.Server.Maps
{
    class GameMap
    {
        private int id;
        private string name;
        private Dictionary<int, Character> players;
        private EnemyRespawnWorker enemyRespawnWorker;
        private TiledMap tileMap;
        private List<SpawnPoint> spawnPoints;
        private Dictionary<int, GameMapObject> mapObjects;
        private Dictionary<int, GameMapPortal> mapPortals;

        // Player Id's should start at a high number to avoid conflicts..
        private int currentObjectId = 100;

        public event EnemyRespawnWorkerEventHandler EnemyRespawnEvent;

        public int Id { get { return id; } set { id = value; } }
        public string Name { get { return name; } set { name = value; } }
        public Dictionary<int, Character> Players { get { return players; } set { players = value; } }
        public TiledMap TileMap { get { return tileMap; } set { tileMap = value; } }
        public List<SpawnPoint> SpawnPoints { get { return spawnPoints; } set { spawnPoints = value; } }
        public Dictionary<int, GameMapObject> MapObjects { get { return mapObjects; } set { mapObjects = value; } }
        public Dictionary<int, GameMapPortal> MapPortals { get { return mapPortals; } set { mapPortals = value; } }

        public GameMap(string filename_)
        {
            players = new Dictionary<int, Character>();
            tileMap = new TiledMap(filename_);

            id = int.Parse(tileMap.Properties["id"].Value);
            name = tileMap.Properties["name"].Value;

            spawnPoints = new List<SpawnPoint>();
            mapObjects = new Dictionary<int, GameMapObject>();
            mapPortals = new Dictionary<int, GameMapPortal>();

            if (tileMap.Objects.ContainsKey("sp"))
            {
                foreach (KeyValuePair<string, TileMapObject> mapObjectSpawnPoints in tileMap.Objects["sp"].Objects)
                {
                    Enemy enemy = EnemyDataProvider.Instance.GetEnemy(int.Parse(mapObjectSpawnPoints.Value.Properties["enemy"].Value));
                    enemy.Direction = EnemyDirection.LEFT;
                    enemy.X = mapObjectSpawnPoints.Value.X;
                    enemy.Y = mapObjectSpawnPoints.Value.Y;

                    SpawnPoint sp = new SpawnPoint(enemy, int.Parse(mapObjectSpawnPoints.Value.Properties["time"].Value));

                    sp.X = mapObjectSpawnPoints.Value.X;
                    sp.Y = mapObjectSpawnPoints.Value.Y;

                    //sp.SpawnEnemy(this);

                    spawnPoints.Add(sp);
                }
            }

            foreach (KeyValuePair<string, TileMapObject> portal in tileMap.Objects["portals"].Objects) 
            {
                GameMapPortal mapPortal = new GameMapPortal(int.Parse(portal.Value.Properties["id"].Value));
                mapPortals.Add(mapPortal.Id, mapPortal);
            }

            //spawnPoints[1].Enemy_.Status = EnemyStatus.DEAD;
            enemyRespawnWorker = new EnemyRespawnWorker(5, this);
            enemyRespawnWorker.Start();
        }

        public void broadcastPacket(NetOutgoingMessage packet) {
            List<NetConnection> toSendTo = new List<NetConnection>();
            foreach (KeyValuePair<int, Character> character in players)
            {
                toSendTo.Add(character.Value.client.Session);
            }
            GameServer.Instance.netServer.SendMessage(packet, toSendTo, NetDeliveryMethod.ReliableOrdered, 1);
        }

        public void broadcastPacket(NetOutgoingMessage packet, int sequenceChannel)
        {
            List<NetConnection> toSendTo = new List<NetConnection>();
            foreach (KeyValuePair<int, Character> character in players)
            {
                toSendTo.Add(character.Value.client.Session);
            }
            GameServer.Instance.netServer.SendMessage(packet, toSendTo, NetDeliveryMethod.ReliableOrdered, sequenceChannel);
        }

        public void AddMapObject(GameMapObject mapobject)
        {
            mapobject.ObjectId = currentObjectId;
            mapObjects.Add(mapobject.ObjectId, mapobject);
            Console.WriteLine("Adding map object: " + mapobject.ObjectId);
            currentObjectId++;
        }

        public GameMapObject GetMapObject(int objectid)
        {
            GameMapObject mapobject = null;

            foreach (KeyValuePair<int, GameMapObject> mapobj in mapObjects)
            {
                if (mapobj.Value.ObjectId == objectid)
                    mapobject = mapobj.Value;
            }

            return mapobject;
        }

        public void AddPlayer(int charid, Character player)
        {
            players.Add(charid, player);
            mapObjects.Add(player.ObjectId, player);
            Console.WriteLine("Adding player map object: " + player.ObjectId);
        }

        public void MovePlayer(Character player)
        {
            players[player.Id].X = player.X;
            players[player.Id].Y = player.Y;
            players[player.Id].Direction = player.Direction;
        }

        public Character GetPlayerById(int id)
        {
            foreach (KeyValuePair<int, Character> player in players)
            {
                if (player.Value.Id == id)
                {
                    return player.Value;
                }
            }

            return null;
        }

        public void SpawnEnemy(Enemy enemy)
        {
            AddMapObject(enemy);
            broadcastPacket(PacketCreator.SpawnEnemy(enemy));
            Console.WriteLine("Spawning enemy " + enemy.ObjectId + " at " + enemy.X +","+enemy.Y);
        }

        public void Respawn()
        {
            foreach (SpawnPoint sp in spawnPoints)
            {
                if (sp.ShouldSpawn())
                {
                    sp.SpawnEnemy(this);
                } 
            }
        }

        #region EnemyRespawnWorker
        class EnemyRespawnWorker
        {
            private Timer timer;
            private int interval;
            private GameMap map;

            public Timer Timer_ { get { return timer; } set { timer = value; } }
            public int Interval { get { return interval; } set { interval = value; } }

            public EnemyRespawnWorker(int spawnTime, GameMap map_)
            {
                interval = spawnTime;
                timer = new Timer(spawnTime);
                timer.Elapsed += new ElapsedEventHandler(RespawnEnemyTimerEvent);
                map = map_;
                map.EnemyRespawnEvent += new EnemyRespawnWorkerEventHandler(EnemyRespawnEvent);
            }

            public void Start()
            {
                timer.Enabled = true;
                timer.Stop();
            }

            private void RespawnEnemyTimerEvent(object obj, ElapsedEventArgs e)
            {
                map.EnemyRespawnEvent(new EnemyRespawnWorkerEventArgs(1));
                //Console.WriteLine("Timer Event: " + e.SignalTime.ToString());
            }

            private void EnemyRespawnEvent(EnemyRespawnWorkerEventArgs e)
            {
                map.Respawn();
                //Console.WriteLine("RespawnEvent");
            }
        }
        public delegate void EnemyRespawnWorkerEventHandler(EnemyRespawnWorkerEventArgs e);
        public class EnemyRespawnWorkerEventArgs {
            private int enemyId;
            public int EnemyId { get { return enemyId; } }

            public EnemyRespawnWorkerEventArgs(int enemyid)
            {
                enemyId = enemyid;
            }
        }
        #endregion
    }
}
