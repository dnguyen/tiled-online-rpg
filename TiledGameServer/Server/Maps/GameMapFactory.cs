﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiledGameServer.Server.Maps
{
    class GameMapFactory
    {
        private Dictionary<string, GameMap> maps = new Dictionary<string,GameMap>();

        // Future: Where to grab map data from
        public GameMapFactory()
        {
        }

        // Switch to map id's in the future?
        public GameMap GetMap(string name)
        {
            GameMap map;
            if (maps.ContainsKey(name))
                map = maps[name];
            else
                map = null;

            // Map has not been loaded yet, load it!
            if (map == null)
            {
                Console.WriteLine("MapFactoryMessage: " + name + " is null, loading map and adding to list");
                string mapName = name;
                map = new GameMap(mapName);
                maps.Add(mapName, map);
            }

            return map;

        }
    }
}
