﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.IO;

namespace TiledGameServer.Server.Maps.TiledMapLoading
{
    class TiledMap
    {
        private int width;
        private int height;
        private int tileWidth;
        private int tileHeight;
        private int gridWidth;
        private int gridHeight;
        private Dictionary<string, TiledLayer> layers;
        private Dictionary<string, ObjectGroup> objectsGroups;
        private Dictionary<string, TileMapProperty> properties;

        public Dictionary<string, TiledLayer> Layers { get { return layers; } set { layers = value; } }
        public Dictionary<string, ObjectGroup> Objects { get { return objectsGroups; } set { objectsGroups = value; } }
        public Dictionary<string, TileMapProperty> Properties { get { return properties; } set { properties = value; } }
        public int Width { get { return width; } set { width = value; } }
        public int Height { get { return height; } set { height = value; } }
        public int GridWidth { get { return gridWidth; } set { gridWidth = value; } }
        public int GridHeight { get { return gridHeight; } set { gridHeight = value; } }

        public TiledMap(string fileName)
        {
            layers = new Dictionary<string, TiledLayer>();
            objectsGroups = new Dictionary<string, ObjectGroup>();
            properties = new Dictionary<string, TileMapProperty>();

            LoadMapFile(fileName);

            gridWidth = tileWidth * width;
            gridHeight = tileHeight * height;
        }

        private void LoadMapFile(string fileName)
        {
            //string path = "data/maps/" + fileName + ".tmx";
            string folderPath = "data/maps/" + fileName;
            if (Directory.Exists(folderPath))
            {
                foreach (string filePath in Directory.GetFiles(folderPath))
                {
                    if (File.Exists(filePath))
                    {
                        XmlDocument mapfile = new XmlDocument();
                        mapfile.Load(folderPath + "/" + fileName + ".tmx");

                        XmlNode mapNode = mapfile.GetElementsByTagName("map")[0];
                        width = int.Parse(mapNode.Attributes["width"].Value);
                        height = int.Parse(mapNode.Attributes["height"].Value);
                        tileWidth = int.Parse(mapNode.Attributes["tilewidth"].Value);
                        tileHeight = int.Parse(mapNode.Attributes["tileheight"].Value);

                        // Load Map Properties - currently doesn't know the difference between map and layer properties. will add layer properties functionality when needed
                        XmlNode propertyNodes = mapfile.GetElementsByTagName("properties")[0];
                        foreach (XmlNode propertyNode in propertyNodes.ChildNodes)
                        {
                            properties.Add(propertyNode.Attributes["name"].Value, new TileMapProperty(propertyNode.Attributes["name"].Value, propertyNode.Attributes["value"].Value));
                        }

                        // Load map layers
                        XmlNodeList layerNodes = mapfile.GetElementsByTagName("layer");
                        foreach (XmlNode layerNode in layerNodes)
                        {
                            TiledLayer tileLayer = new TiledLayer(layerNode.Attributes["name"].Value, int.Parse(layerNode.Attributes["width"].Value), int.Parse(layerNode.Attributes["height"].Value));
                            foreach (XmlNode dataNode in layerNode.ChildNodes)
                            {
                                int row = 0;
                                int col = 0;
                                foreach (XmlNode tileNode in dataNode.ChildNodes)
                                {
                                    Tile newTile = new Tile(int.Parse(tileNode.Attributes["gid"].Value), row, col);
                                    tileLayer.Data[row, col] = newTile;

                                    if (col < tileLayer.Width - 1)
                                        col++;
                                    else
                                    {
                                        col = 0;

                                        if (row < tileLayer.Height - 1)
                                            row++;
                                    }
                                }
                            }
                            layers.Add(tileLayer.Name, tileLayer);
                            //tileLayer.Print();
                        }

                        XmlNodeList objectGroups = mapfile.GetElementsByTagName("objectgroup");

                        foreach (XmlNode objectGroupNode in objectGroups)
                        {
                            ObjectGroup objectGroup = new ObjectGroup(objectGroupNode.Attributes["name"].Value);

                            foreach (XmlNode objectNode in objectGroupNode.ChildNodes)
                            {
                                TileMapObject mapObject = new TileMapObject(objectNode.Attributes["name"].Value, 
                                    objectNode.Attributes["type"].Value,
                                    int.Parse(objectNode.Attributes["x"].Value),
                                    int.Parse(objectNode.Attributes["y"].Value),
                                    int.Parse(objectNode.Attributes["gid"].Value));

                                foreach (XmlNode propertiesNode in objectNode.ChildNodes)
                                {
                                    foreach (XmlNode propertyNode in propertiesNode.ChildNodes)
                                    {
                                        mapObject.Properties.Add(
                                            propertyNode.Attributes["name"].Value, 
                                            new Property(propertyNode.Attributes["name"].Value, 
                                                propertyNode.Attributes["value"].Value));
                                    }
                                }

                                objectGroup.Objects.Add(mapObject.Name, mapObject);
                            }
                            objectsGroups.Add(objectGroup.Name, objectGroup);
                        }
                    }
                }
            }
        }
    }
}
