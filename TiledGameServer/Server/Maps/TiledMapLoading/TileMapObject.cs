﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiledGameServer.Server.Maps.TiledMapLoading
{
    class TileMapObject
    {
        private string name;
        private string type;
        private int x;
        private int y;
        private int width;
        private int height;
        private int gid;
        private Dictionary<string, Property> properties;

        #region Properties
        public string Name { get { return name; } set { name = value; } }
        public string Type { get { return type; } set { type = value; } }
        public int X { get { return x; } set { x = value; } }
        public int Y { get { return y; } set { y = value; } }
        public int Width { get { return width; } set { width = value; } }
        public int Height { get { return height; } set { height = value; } }
        public int GID { get { return gid; } set { gid = value; } }
        public Dictionary<string, Property> Properties { get { return properties; } set { properties = value; } }
        #endregion

        public TileMapObject(string name_, string type_, int x_, int y_, int gid_)
        {
            name = name_;
            type = type_;
            x = x_;
            y = y_;
            gid = gid_;
            properties = new Dictionary<string, Property>();
        }
    }
}
