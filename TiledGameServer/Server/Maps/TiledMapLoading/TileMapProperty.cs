﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiledGameServer.Server.Maps.TiledMapLoading
{
    class TileMapProperty
    {
        private string name;
        private string value_;

        public string Name { get { return name; } set { name = value; } }
        public string Value { get { return value_; } set { value_ = value; } }

        public TileMapProperty(string name, string value_)
        {
            this.name = name;
            this.value_ = value_;
        }
    }
}
