﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiledGameServer.Server.Maps.TiledMapLoading
{
    class ObjectGroup
    {
        public string Name { get; set; }
        public Dictionary<string, TileMapObject> Objects { get; set; }

        public ObjectGroup(string name)
        {
            Name = name;
            Objects = new Dictionary<string, TileMapObject>();
        }
    }
}
