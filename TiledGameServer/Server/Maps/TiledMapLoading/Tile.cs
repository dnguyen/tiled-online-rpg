﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace TiledGameServer.Server.Maps.TiledMapLoading
{
    class Tile
    {
        private int id;
        private Rectangle bounds;
        private int x;
        private int y;
        private int width = 32;
        private int height = 32;

        public int Id { get { return id; } }
        public Rectangle Bounds { get { return bounds; } }

        public Tile(int id_, int x_, int y_)
        {
            id = id_;
            x = x_ * height;
            y = y_ * width;
            bounds = new Rectangle(y, x, width, height);
        }
    }
}
