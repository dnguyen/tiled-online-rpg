﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiledGameServer.Server.Maps.TiledMapLoading
{
    class TiledLayer
    {
        private string name;
        private int width;
        private int height;
        private Tile[,] data;

        public string Name { get { return name; } set { name = value; } }
        public Tile[,] Data { get { return data; } set { data = value; } }
        public int Width { get { return width; } }
        public int Height { get { return height; } }

        public TiledLayer(string name_, int width_, int height_)
        {
            name = name_;
            width = width_;
            height = height_;
            data = new Tile[height_, width_];
        }

        public void Print()
        {
            Console.WriteLine("Layer name: " + name);
            for (int x = 0; x < Height; x++)
            {
                for (int y = 0; y < width; y++)
                {
                    Console.Write(data[x, y].Id);
                }
                Console.WriteLine("");
            }
        }
    }
}
