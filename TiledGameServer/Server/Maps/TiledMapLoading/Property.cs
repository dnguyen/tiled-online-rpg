﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiledGameServer.Server.Maps.TiledMapLoading
{
    class Property
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public Property(string name, string value)
        {
            Name = name;
            Value = value;
        }
    }
}
