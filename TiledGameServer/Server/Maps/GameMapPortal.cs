﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TiledGameServer.Tools;

namespace TiledGameServer.Server.Maps
{
    class GameMapPortal
    {
        private int id;
        private String name;
        private Point position;
        private int destination;

        public int Id { get { return id; } set { id = value; } }
        public String Name { get { return name; } set { name = value; } }
        public Point Position { get { return position; } set { position = value; } }
        public int Destination { get { return destination; } set { destination = value; } }

        public GameMapPortal(int id_)
        {
            id = id_;
        }
    }
}
