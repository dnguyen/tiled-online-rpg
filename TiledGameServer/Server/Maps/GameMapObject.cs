﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TiledGameServer.Tools;
using System.Drawing;

namespace TiledGameServer.Server.Maps
{
    public enum GameMapObjectType
    {
        PLAYER = 0,
        ENEMY = 1
    }

    class GameMapObject
    {
        private int objectId;
        private TiledGameServer.Tools.Point position;
        private TiledGameServer.Tools.Point velocity;
        private bool collided;
        private GameMapObjectType objectType;

        #region Properties
        public int ObjectId { get { return objectId; } set { objectId = value; } }
        public TiledGameServer.Tools.Point Position { 
            get { return position; } 
            set { 
                position = value;
                this.Bounds = new Rectangle(position.X, position.Y, 32, 32);
            } 
        }
        public int X { get { return position.X; } set { position.X = value; } }
        public int Y { get { return position.Y; } set { position.Y = value; } }
        public GameMapObjectType ObjectType { get { return objectType; } set { objectType = value; } }
        public bool Collided { get { return collided; } set { collided = value; } }
        public Rectangle Bounds { get; set; }
        #endregion
    }
}
