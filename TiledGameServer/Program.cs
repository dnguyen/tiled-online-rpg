﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;
using System.Threading;

namespace TiledGameServer
{
    class Program
    {

        static void Main(string[] args)
        {
            GameServer.newInstance();
            GameServer.Instance.Run();
        }
    }
    #region blah
    /*           // schedule initial sending of position updates
            double nextSendUpdates = NetTime.Now;
            string playerName = "";

            // run until escape is pressed
            while (!Console.KeyAvailable || Console.ReadKey().Key != ConsoleKey.Escape)
            {
                NetIncomingMessage msg;
                while ((msg = server.ReadMessage()) != null)
                {
                    switch (msg.MessageType)
                    {
                        case NetIncomingMessageType.DiscoveryRequest:
                            //
                            // Server received a discovery request from a client; send a discovery response (with no extra data attached)
                            //
                            server.SendDiscoveryResponse(null, msg.SenderEndpoint);
                            break;
                        case NetIncomingMessageType.VerboseDebugMessage:
                        case NetIncomingMessageType.DebugMessage:
                        case NetIncomingMessageType.WarningMessage:
                        case NetIncomingMessageType.ErrorMessage:
                            //
                            // Just print diagnostic messages to console
                            //
                            Console.WriteLine(msg.ReadString());
                            break;
                        case NetIncomingMessageType.StatusChanged:
                            NetConnectionStatus status = (NetConnectionStatus)msg.ReadByte();
                            if (status == NetConnectionStatus.Connected)
                            {
                                //
                                // A new player just connected!
                                //
                                Console.WriteLine(NetUtility.ToHexString(msg.SenderConnection.RemoteUniqueIdentifier) + " connected!");

                                // randomize his position and store in connection tag
                                msg.SenderConnection.Tag = new int[] {
									NetRandom.Instance.Next(10, 100),
									NetRandom.Instance.Next(10, 100)
								};
                            }

                            break;
                        case NetIncomingMessageType.Data:
                            // Get first byte -> get Id -> look up id through enum? -> points to correct handler
                            //
                            // The client sent input to the server
                            //
                            short packetid = msg.ReadInt16();
                            Console.WriteLine(packetid);


                            //playerName = msg.ReadString();
                            //int xinput = msg.ReadInt32();
                            //int yinput = msg.ReadInt32();
                            //Console.WriteLine("Recieved from " + playerName + ": (" + xinput + "," + yinput + ")");
                            //int[] pos = msg.SenderConnection.Tag as int[];

                            // fancy movement logic goes here; we just append input to position
                            //pos[0] = xinput;
                            //pos[1] = yinput;
                            break;
                    }

                    //
                    // send position updates 30 times per second
                    //
                    double now = NetTime.Now;
                    if (now > nextSendUpdates)
                    {
                        // Yes, it's time to send position updates

                        // for each player...
                        foreach (NetConnection player in server.Connections)
                        {
                            // ... send information about every other player (actually including self)
                            foreach (NetConnection otherPlayer in server.Connections)
                            {
                                // send position update about 'otherPlayer' to 'player'
                                NetOutgoingMessage om = server.CreateMessage();

                                // write who this position is for
                                om.Write(otherPlayer.RemoteUniqueIdentifier);

                                if (otherPlayer.Tag == null)
                                    otherPlayer.Tag = new int[2];

                                int[] pos = otherPlayer.Tag as int[];
                                om.Write(playerName);
                                om.Write(pos[0]);
                                om.Write(pos[1]);

                                // send message
                                server.SendMessage(om, player, NetDeliveryMethod.Unreliable);
                            }
                        }

                        // schedule next update
                        nextSendUpdates += (1.0 / 30.0);
                    }
                }

                // sleep to allow other processes to run smoothly
                Thread.Sleep(1);
            }

            server.Shutdown("app exiting");*/
    #endregion
}
