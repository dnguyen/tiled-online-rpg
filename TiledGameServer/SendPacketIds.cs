﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TiledGameServer
{
    // List of ALL packet id's that are sent to the client.
    public enum SendPacketIds
    {
        PLAYER_LOGIN = 0x01,
        SHOW_CHARACTER = 0x02,
        SPAWN_PLAYER = 0x03,
        PLAYER_MOVE = 0x04,
        PLAYER_CHAT = 0x05,
        PLAYER_EQUIP_ITEM = 0x06,
        MELEE_ATTACK = 0x07,
        RANGED_ATTACK = 0x08,
        SPAWN_ENEMY = 0x09,
        KILL_ENEMY = 0x10,
        SPAWN_PLAYER_MAP_OBJECT = 0x31
    }
}
